#include "Weapons/WeaponSpawner.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "TDCharacter.h"
#include "Components//WeaponComponent.h"
#include "Weapons/TDHelper_Weapons.h"
#include "TimerManager.h"

// Sets default values
AWeaponSpawner::AWeaponSpawner()
{
    // Set this character to call Tick() every frame. You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;

    RootComponent = CollisionVolume = CreateDefaultSubobject<UBoxComponent>("CollisionVolume");
    CollisionVolume->OnComponentBeginOverlap.AddDynamic(this, &AWeaponSpawner::OnOverlapBegin);
    CollisionVolume->SetBoxExtent(FVector(18.0f, 12.0f, 32.0f));

    PadMesh = CreateDefaultSubobject<UStaticMeshComponent>("PadMesh");
    PadMesh->SetupAttachment(RootComponent);

    WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>("WeaponMesh");
    WeaponMesh->SetupAttachment(RootComponent);

    RespawnCoolDown = 10.0f;
}

void AWeaponSpawner::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void AWeaponSpawner::BeginPlay()
{
    Super::BeginPlay();
}

void AWeaponSpawner::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
    int32 OtherBodyIndex, bool bFromSwee, const FHitResult& SweepResult)
{
    if (!OtherActor) return;
    TryToAddWeaponToCharacter(OtherActor);
}

bool AWeaponSpawner::TryToAddWeaponToCharacter(const AActor* Actor)
{
    if (!bCanTakeWeapon) return false;

    auto WeaponComponent = UTDHelper_Weapons::GetWeaponComponentFromActor(Actor);
    if (!WeaponComponent) return false;

    if (WeaponComponent->TryToAddWeapon(WeaponClass))
    {
        bCanTakeWeapon = false;

        if (!bInfiniteRespawn)
        {
            WeaponMesh->DestroyComponent(true);
            return false;
        }

        WeaponMesh->SetVisibility(false);

        /*don't clear timer if player make a lot of attempts*/
        if (bResetingCooldown) return false;

        GetWorldTimerManager().SetTimer(ResetCooldownTimer, this,  //
            &AWeaponSpawner::ResetCooldown,                        //
            RespawnCoolDown, false);
        bResetingCooldown = true;
    }

    return true;
}

void AWeaponSpawner::ResetCooldown()
{
    bCanTakeWeapon = true;
    bResetingCooldown = false;

    WeaponMesh->SetVisibility(true);
}
