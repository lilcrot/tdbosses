// Top Down Bosses. Project for learning

#include "Weapons/Ranged/RangedWeapon.h"
#include "TDCoreTypes.h"

DEFINE_LOG_CATEGORY_STATIC(LogRangedWeapon, All, All);

ARangedWeapon::ARangedWeapon()
{
    WeaponFireData.Type = EWeaponTypes::Ranged;
}
