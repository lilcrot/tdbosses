// Top Down Bosses. Project for learning

#include "Weapons/TDWeaponDebugSettings.h"
#include "Misc/App.h"

#include UE_INLINE_GENERATED_CPP_BY_NAME(TDWeaponDebugSettings)

UTDWeaponDebugSettings::UTDWeaponDebugSettings() {}

FName UTDWeaponDebugSettings::GetCategoryName() const
{
    return FApp::GetProjectName();
}
