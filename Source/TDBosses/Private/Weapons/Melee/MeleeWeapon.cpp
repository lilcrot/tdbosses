// Top Down Bosses. Project for learning

#include "Weapons/Melee/MeleeWeapon.h"
#include "GAS/Abilities/Weapon/Melee/GameplayAbility_MeleeFireBase.h"
#include "TDCoreTypes.h"
#include "Kismet/KismetMathLibrary.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogMeleeWeapon, All, All);

AMeleeWeapon::AMeleeWeapon()
{
    WeaponFireData.Type = EWeaponTypes::Melee;
    WeaponFireData.bAutoFire = false;
}

void AMeleeWeapon::BeginPlay()
{
    Super::BeginPlay();
    checkf(GetAmmoData().bInfinite, TEXT("Infinite 'bullets'/'attacks' for melee have not been configured!"));

    checkf(MeshComponent->GetSocketByName(WeaponFireData.BladeStartSocketName),
        TEXT("BladeStartSocket(with parametrized name by WeaponFireData: BladeStartSocketName) not found in weapon mesh"));

    checkf(MeshComponent->GetSocketByName(WeaponFireData.BladeEndSocketName),
        TEXT("BladeEndSocket(with parametrized name by WeaponFireData: BladeEndSocketName) not found in weapon mesh"));
}
void AMeleeWeapon::OnEndAttack()
{
    Super::StopFire();
}

const FVector AMeleeWeapon::GetStartPointOfBlade() const
{
    const FTransform SocketTransform = MeshComponent->GetSocketTransform(WeaponFireData.BladeStartSocketName);
    return SocketTransform.GetLocation();
}
const FVector AMeleeWeapon::GetEndPointOfBlade() const
{
    const FTransform SocketTransform = MeshComponent->GetSocketTransform(WeaponFireData.BladeEndSocketName);
    return SocketTransform.GetLocation();
}

void AMeleeWeapon::StartTrailVFX()
{
    const FVector StartBlade = GetStartPointOfBlade();
    const FVector EndBlade = GetEndPointOfBlade();

    const FVector SpawnLocation = (StartBlade + EndBlade) / 2.0f;  // center of blade
    const FRotator SpawnRotation = UKismetMathLibrary::MakeRotFromZ(StartBlade - EndBlade);

    TrailNiagaraComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(TrailNiagaraSystem, GetMesh(), "",         //
        SpawnLocation, SpawnRotation, EAttachLocation::KeepWorldPosition, false, true, ENCPoolMethod::None, true);  //

    if (!IsValid(TrailNiagaraComponent)) return;
    constexpr float TrailWidthDivider = 1.2f;  // more beauty
    TrailNiagaraComponent->SetFloatParameter("TrailWidth", UKismetMathLibrary::VSize(StartBlade - EndBlade) / TrailWidthDivider);
    TrailNiagaraComponent->SetColorParameter("TrailColor", TrailColor);
}

void AMeleeWeapon::StopTrailVFX()
{
    if (!IsValid(TrailNiagaraComponent)) return;
    TrailNiagaraComponent->DestroyComponent();
    TrailNiagaraComponent = nullptr;
}
