// Top Down Bosses. Project for learning

#include "Weapons/TDBaseWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Weapons/TDHelper_Weapons.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "GAS/TDAbilitySystemComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogBaseWeapon, All, All);

// @param NameFunc: name must match with weapon declaration in header
// (that is this func must be in base weapon header)
// @param NameAbilityVariable: based on this UClass FireAction will be executed by GA
#define DEFINE_FireAbility(NameFunc, NameAbilityVariable)                                                                                  \
    bool ATDBaseWeapon::NameFunc()                                                                                                         \
    {                                                                                                                                      \
        auto* ASC = GetAbilitySystemComponent();                                                                                           \
        if (!ASC) return false;                                                                                                            \
        bFiring = ASC->TryActivateAbilityByClass(NameAbilityVariable, true);                                                               \
                                                                                                                                           \
        return bFiring;                                                                                                                    \
    }

ATDBaseWeapon::ATDBaseWeapon()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;

    MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>("MeshComponent");
    MeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    SetRootComponent(MeshComponent);
}

void ATDBaseWeapon::BeginPlay()
{
    Super::BeginPlay();
    check(MeshComponent);

    AmmoData.SetCurrentBullets(AmmoData.MaxBullets);
}

void ATDBaseWeapon::SetVisibilityHandle(bool NewVisiblity) const
{
    checkf(MeshComponent, TEXT("Weapon mesh isn't valid"));
    MeshComponent->SetVisibility(NewVisiblity);
}

const FTransform ATDBaseWeapon::GetMuzzleSocketTransform() const
{
    checkf(MeshComponent, TEXT("Weapon mesh isn't valid"));
    return MeshComponent->GetSocketTransform(WeaponFireData.MuzzleSocketName);
}

void ATDBaseWeapon::OnAttachedToOwner()
{
    /* init GAS & fire abilities */
    GiveFireAbility(DefaultFireInfo);
    GiveFireAbility(SpecialFireInfo);
}

bool ATDBaseWeapon::GiveFireAbility(const TSubclassOf<UGameplayAbility_FireBase> FireInfo)
{
    auto* ASC = GetAbilitySystemComponent();
    if (!ASC) return false;

    constexpr int32 InLevel = 0;
    constexpr int32 InInputID = -1;  // this fire ability will activate by triggered input action that must declared declared in owner

    ASC->GiveAbility(FGameplayAbilitySpec(FireInfo, InLevel, InInputID, this));
    return true;
}

DEFINE_FireAbility(DefaultFire, DefaultFireInfo);
DEFINE_FireAbility(SpecialFire, SpecialFireInfo);

void ATDBaseWeapon::StopFire()
{
    FGameplayEventData Data;
    Data.EventTag = StopFireTag;
    Data.Instigator = GetInstigator();
    Data.Target = this;

    UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(this, Data.EventTag, Data);
    bFiring = false;

    UWeaponComponent* WeaponComponent = UTDHelper_Weapons::GetWeaponComponentFromActor_Check(GetOwner());
    WeaponComponent->OnCurrentWeapon_StopFire.Broadcast();
}

void ATDBaseWeapon::TryReload() {}

void ATDBaseWeapon::OnEquipped()
{
    auto Character = Cast<ACharacter>(GetOwner());
    if (!Character) return;

    // transfer anim link
    Character->GetMesh()->LinkAnimClassLayers(WeaponAnimationData.EquipAnimLayer);
}

void ATDBaseWeapon::OnUnequipped()
{
    auto Character = Cast<ACharacter>(GetOwner());
    if (!Character) return;

    StopFire();
}

AController* ATDBaseWeapon::GetController() const
{
    const auto Pawn = Cast<APawn>(GetOwner());
    return Pawn ? Pawn->GetController() : nullptr;
}

UAbilitySystemComponent* ATDBaseWeapon::GetAbilitySystemComponent() const
{
    UWeaponComponent* WeaponComponent = UTDHelper_Weapons::GetWeaponComponentFromActor_Check(GetOwner());
    return WeaponComponent->GetAbilitySystemComponent();
}
