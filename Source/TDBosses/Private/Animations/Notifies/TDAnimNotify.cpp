// Top Down Bosses. Project for learning

#include "Animations/Notifies/TDAnimNotify.h"

void UTDAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    OnNotifed.Broadcast(MeshComp, Animation, EventReference);
    BP_OnNotified.Broadcast(MeshComp, Animation, EventReference);
    Super::Notify(MeshComp, Animation, EventReference);
}
