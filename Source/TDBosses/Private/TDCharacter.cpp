#include "TDCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/HealthComponent.h"
#include "Components/WeaponComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Weapons/TDBaseWeapon.h"
#include "TDCoreTypes.h"
#include "Player/TargettingNetHelper.h"
#include "Components/TimelineComponent.h"
#include "Kismet/KismetMathLibrary.h"

#include "GAS/TDAbilitySystemComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDCharacter, All, All);

// Sets default values
ATDCharacter::ATDCharacter()
{
    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    GetCharacterMovement()->bOrientRotationToMovement = true;             // Character moves in the direction of input...
    GetCharacterMovement()->RotationRate = FRotator(0.0f, 400.0f, 0.0f);  // ...at this rotation rate

    HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");
    WeaponComponent = CreateDefaultSubobject<UWeaponComponent>("WeaponComponent");
    WeaponComponent->SetIsReplicated(true);  // for GAS give abilities

    TDAbilitySystemComponent = CreateDefaultSubobject<UTDAbilitySystemComponent>("TDAbilitySystemComponent");

    bAbilityInitialized = false;
}

// Called when the game starts or when spawned
void ATDCharacter::BeginPlay()
{
    Super::BeginPlay();

    check(TDAbilitySystemComponent);
    check(WeaponComponent);
    check(HealthComponent);
    checkf(RotateWithLerpCurveFloat, TEXT("RotateWithLerpCurveFloat forgotten to set"));

    HealthComponent->OnDeath.AddDynamic(this, &ThisClass::OnDeath);

    BindTimelines();

    /* Spawn targetting net helper that interacts and saves enemies */
    TargettingNetHelper = ATargettingNetHelper::SpawnTargettingNetHelper(this);
    checkf(TargettingNetHelper, TEXT("TargettingNetHelper isn't valid!"));
}

void ATDCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);
}

void ATDCharacter::PossessedBy(AController* NewController)
{
    Super::PossessedBy(NewController);

    if (auto* ASC = GetAbilitySystemComponent())
    {
        ASC->InitAbilityActorInfo(this, this);
        AddStartupGameplayAbilities();
    }
}

void ATDCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
    RotateToEnemyTimeLine.TickTimeline(DeltaSeconds);
    RotateCharacterWithLerpTimeLine.TickTimeline(DeltaSeconds);
}

FVector ATDCharacter::GetWeaponSocketLocation() const
{
    const FWeaponEquipAttachSocket SocketStruct = GetWeaponSocketStruct();
    return GetMesh()->GetSocketLocation(SocketStruct.SocketName);
}

void ATDCharacter::OnDeath()
{
    UE_LOG(LogTDCharacter, Display, TEXT("%s died"), *GetName());
    SetLifeSpan(DeathVaribles.LifeSpanOnDeath);
    GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    WeaponComponent->StopFire();
    WeaponComponent->HideAllWeapon();

    UAnimMontage* DeathMontage = DeathVaribles.DeathMontages[FMath::RandRange(0, DeathVaribles.DeathMontages.Num() - 1)];
    PlayAnimMontage(DeathMontage);

    FTimerHandle DelayTimerHandle;
    GetWorld()->GetTimerManager().SetTimer(
        DelayTimerHandle,
        [&]()
        {
            Ragdoll();
            DeathVisual();
        },
        FMath::RandRange(0.1f, 0.3f), false);
}

void ATDCharacter::Ragdoll()
{
    const auto MyMesh = GetMesh();
    if (!MyMesh) return;

    MyMesh->SetCollisionProfileName("Ragdoll", true);
    MyMesh->SetAllBodiesBelowSimulatePhysics(DeathVaribles.RagdollImpulseBone, true);

    const auto LastUpdateVelocity = GetCharacterMovement()->GetLastUpdateVelocity();
    const FVector Impulse = LastUpdateVelocity + LastUpdateVelocity.GetSafeNormal() * DeathVaribles.ImpulseStrenght;
    MyMesh->AddImpulse(Impulse, DeathVaribles.RagdollImpulseBone, true);
}

bool ATDCharacter::GetIsFiring() const
{
    checkf(WeaponComponent, TEXT("Weapon component isn't valid"));
    return WeaponComponent->GetIsFiring();
}

void ATDCharacter::DefaultFire()
{
    checkf(WeaponComponent, TEXT("Weapon component isn't valid"));
    WeaponComponent->DefaultFire();
}
void ATDCharacter::SpecialFire()
{
    checkf(WeaponComponent, TEXT("Weapon component isn't valid"));
    WeaponComponent->SpecialFire();
}
void ATDCharacter::StopFire()
{
    checkf(WeaponComponent, TEXT("Weapon component isn't valid"));
    WeaponComponent->StopFire();
    bLerpRotationInProgress = false;  // player can cancel rotation(that executes from TDPlayerController) so we must set it clearly
}

void ATDCharacter::RotateCharacterWithLerp(FRotator NewRotationIn)
{
    bLerpRotationInProgress = true;
    NewRotationTimeLine = FRotator(0.0, (NewRotationIn.Yaw), 0.0);  // TDBosses game design implies rotation only on yaw
    RotateCharacterWithLerpTimeLine.PlayFromStart();
}

void ATDCharacter::RotateToEnemyWithLerp(const FRotator NewRotationIn)
{
    bLerpRotationInProgress = true;
    NewRotationTimeLine = FRotator(0.0, (NewRotationIn.Yaw), 0.0);  // TDBosses game design implies rotation only on yaw
    RotateToEnemyTimeLine.PlayFromStart();
}

void ATDCharacter::UpdateRotateWithLerp(float AlphaAmount)
{
    if (!bLerpRotationInProgress)  // canceled rotation
    {
        ClearRotationWithLerp();
        return;
    }
    const FRotator SetTo = UKismetMathLibrary::RLerp(GetActorRotation(), NewRotationTimeLine, AlphaAmount, true);
    SetActorRotation(SetTo);
}

void ATDCharacter::ClearRotationWithLerp()
{
    NewRotationTimeLine = FRotator::ZeroRotator;
    bLerpRotationInProgress = false;
    RotateCharacterWithLerpTimeLine.Stop();
    RotateToEnemyTimeLine.Stop();
}

void ATDCharacter::OnCharacterRotationToEnemyFinished()
{
    OnCharacterRotationToEnemyEnd.Broadcast(NewRotationTimeLine);
    ClearRotationWithLerp();
}

UAbilitySystemComponent* ATDCharacter::GetAbilitySystemComponent() const
{
    return TDAbilitySystemComponent;
}

UTDAbilitySystemComponent* ATDCharacter::GetTDAbilitySystemComponent() const
{
    return Cast<UTDAbilitySystemComponent>(GetAbilitySystemComponent());
}

void ATDCharacter::AddStartupGameplayAbilities()
{
    if (bAbilityInitialized) return;
    checkf(GetAbilitySystemComponent(), TEXT("AbilitySystemComponent isn't valid"));
}

bool ATDCharacter::BindTimelines()
{
    FOnTimelineFloat RotateWithLerpInProgress;
    RotateWithLerpInProgress.BindUFunction(this, FName("UpdateRotateWithLerp"));

    RotateCharacterWithLerpTimeLine.AddInterpFloat(RotateWithLerpCurveFloat, RotateWithLerpInProgress);
    RotateToEnemyTimeLine.AddInterpFloat(RotateWithLerpCurveFloat, RotateWithLerpInProgress);

    FOnTimelineEvent CharacterRotationFinished;
    CharacterRotationFinished.BindUFunction(this, FName("ClearRotationWithLerp"));
    RotateCharacterWithLerpTimeLine.SetTimelineFinishedFunc(CharacterRotationFinished);

    FOnTimelineEvent RotateToEnemyFinished;
    RotateToEnemyFinished.BindUFunction(this, FName("OnCharacterRotationToEnemyFinished"));
    RotateToEnemyTimeLine.SetTimelineFinishedFunc(RotateToEnemyFinished);
    RotateToEnemyTimeLine.SetTimelineFinishedFunc(CharacterRotationFinished);

    return true;
}

const ATargettingNetHelper* ATDCharacter::GetTargettingNetHelper() const
{
    return TargettingNetHelper;
}

TArray<AActor*> ATDCharacter::GetTargettingOverlapped() const
{
    return TargettingNetHelper ? TargettingNetHelper->GetOverlappedTargets() : TArray<AActor*>();
}

// setupped in blueprints
void ATDCharacter::DeathVisual_Implementation() {}
