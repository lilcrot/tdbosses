#if WITH_AUTOMATION_TESTS

#include "Tests/PlayerTests.h"
#include "Kismet/GameplayStatics.h"
#include "Player/PlayerCharacter.h"
#include "Player/TDPlayerController.h"
#include "Components/HealthComponent.h"
#include "Engine/DamageEvents.h"
#include "Misc/OutputDeviceNull.h"
#include "Tests/Utils/JsonUtils.h"
#include "Weapons/TDHelper_Weapons.h"

using namespace Test;

// TEST_MAP implied (in our case it's "/Game/TDBosses/Tests/TestPlayerEnemy"):
// 1) 1 target dummy or character with health component (no more, our test exepect 1 target)
// 2) Weapon spawner
// 3) Game mode with testable default pawn

DEFINE_LOG_CATEGORY_STATIC(LogTestCharacter, All, All);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FPlayerCanFindTarget, "TDBosses.Player.CanFindTarget",
    EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter | EAutomationTestFlags::HighPriority);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FPlayerCanDie, "TDBosses.Player.CanDie",
    EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter | EAutomationTestFlags::HighPriority);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FPlayerCanKill, "TDBosses.Player.CanKill",
    EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter | EAutomationTestFlags::HighPriority);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FPlayerSimulateInput, "TDBosses.Player.SimulateInput",
    EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter | EAutomationTestFlags::HighPriority);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FPlayerCanGetWeapon, "TDBosses.Player.CanGetWeapon",
    EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter | EAutomationTestFlags::HighPriority);

namespace
{
constexpr char* CharacterTestBPName = "Blueprint'/Game/TDBosses/Tests/BP_TestPlayerCharacter.BP_TestPlayerCharacter'";

// Simulate inputs and json names for it
constexpr char* CharacterCanKill_JsonName = "CharacterCanKill.json";
constexpr char* CharacterCanGetWeapon_JsonName = "CharacterCanGetWeaponInput.json";
constexpr char* SimulateInputTest_JsonName = "CharacterTestInput.json";

}  // namespace

bool FPlayerCanFindTarget::RunTest(const FString& Parameters)
{
    LevelScope("/Game/TDBosses/Tests/TestPlayerEnemy");

    const UWorld* World = GetTestGameWorld();
    if (!TestNotNull("World exists", World)) return false;

    TArray<AActor*> PlayerActorsOut;
    UGameplayStatics::GetAllActorsOfClass(World, APlayerCharacter::StaticClass(), PlayerActorsOut);
    APlayerCharacter* MyPlayer = Cast<APlayerCharacter>(PlayerActorsOut[0]);
    if (!TestNotNull("Player exists", MyPlayer)) return false;

    const auto Targets = MyPlayer->GetTargettingOverlapped();
    if (!TestNotNull("Target is null", Targets[0])) return false;

    return true;
}

bool FPlayerCanDie::RunTest(const FString& Parameters)
{
    const auto Level = LevelScope("/Game/TDBosses/Tests/TestPlayerEnemy");
    UWorld* World = GetTestGameWorld();
    if (!TestNotNull("World exists", World)) return false;

    APlayerCharacter* MyPlayer = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(World, 0));
    if (!TestNotNull("Player exists", MyPlayer)) return false;

    auto* TargettingSystem = MyPlayer->GetTargettingNetHelper();
    if (!TestNotNull("Player hasn't TargetSystem but it implied by game design and code!", TargettingSystem)) return false;

    const auto HealthComponent = MyPlayer->FindComponentByClass<UHealthComponent>();
    if (!TestNotNull("HealthComponent doesn't find", HealthComponent)) return false;

    const int32 Health = HealthComponent->GetMaxHealth();
    HealthComponent->SetHealth(Health);
    const FString HealthEqualMsg = FString::Printf(TEXT("Health is %i"), Health);
    TestEqual(HealthEqualMsg, HealthComponent->GetHealth(), Health);

    ADD_LATENT_AUTOMATION_COMMAND(FWaitLatentCommand(2.0f));  // init delay for get weapon and other action in begin play

    const float DamageAmount = float(Health);
    MyPlayer->TakeDamage(Health, FDamageEvent{}, nullptr, nullptr);
    TestEqual("Health is zero", HealthComponent->GetHealth(), 0);
    TestTrueExpr(HealthComponent->IsDead());
    const float LifeSpanOnDeath = MyPlayer->GetLifeSpan();

    constexpr float MinimalRequiredTimeToDeathVisual = 3.0f;  // also takes into account slow motion that released in visual
    ADD_LATENT_AUTOMATION_COMMAND(FDelayedFunctionLatentCommand(
        [MyPlayer, TargettingSystem]()
        {
            if (IsValid(MyPlayer))
            {
                UE_LOG(LogTestCharacter, Error, TEXT("Player wasn't die"));
            }
            if (IsValid(TargettingSystem))
            {
                UE_LOG(LogTestCharacter, Error, TEXT("TargettingSystem wasn't delete after death of owner"));
            }
        },
        LifeSpanOnDeath + MinimalRequiredTimeToDeathVisual));

    return true;
}

bool FPlayerCanKill::RunTest(const FString& Parameters)
{
    const auto Level = LevelScope("/Game/TDBosses/Tests/TestPlayerEnemy");
    UWorld* World = GetTestGameWorld();
    if (!TestNotNull("World exists", World)) return false;

    APlayerCharacter* MyPlayer = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(World, 0));
    if (!TestNotNull("Player exists", MyPlayer)) return false;

    const TArray<AActor*> Targets = MyPlayer->GetTargettingOverlapped();
    const AActor* Target = nullptr;
    const UHealthComponent* TargetHealthComponent = nullptr;

    for (const AActor* T : Targets)
    {
        if (const UHealthComponent* HealthComponent = T->FindComponentByClass<UHealthComponent>())
        {
            Target = T;
            TargetHealthComponent = HealthComponent;
            break;
        }
    }

    if (!TestNotNull("Target is null", Target)) return false;
    if (!TestNotNull("TargetHealthComponent is null", TargetHealthComponent)) return false;

    APlayerController* PlayerController = UGameplayStatics::GetPlayerController(World, 0);
    if (!TestNotNull("Player controller exists", PlayerController)) return false;

    const FString FileName = GetTestDataDir().Append(CharacterCanKill_JsonName);
    FInputData InputData;
    if (!JsonUtils::ReadInputData(FileName, InputData)) return false;
    if (!TestTrue("Input data is not empty", InputData.Bindings.Num() > 0)) return false;

    MyPlayer->SetActorTransform(InputData.InitialTransform);
    PlayerController->SetControlRotation(InputData.InitialTransform.Rotator());

    ADD_LATENT_AUTOMATION_COMMAND(FSimulateInputActionsLatentCommand(World, Cast<UEnhancedInputComponent>(MyPlayer->InputComponent),  //
        Cast<UEnhancedPlayerInput>(PlayerController->PlayerInput), InputData.Bindings));
    // now we must kill enemy/dummy by simulating inputs

    // we must make it from latent command else check will be processed first and only then simulate inputs
    ADD_LATENT_AUTOMATION_COMMAND(FDelayedFunctionLatentCommand(
        [TargetHealthComponent]()
        {
            /*if TargetHealthComponent isn't valid => target died OR TargetHealthComponent is valid and IsDead() return true => target
             * died*/
            if (IsValid(TargetHealthComponent))
            {
                if (!TargetHealthComponent->IsDead())
                {
                    UE_LOG(LogTestCharacter, Error, TEXT("TargetHealthComponent isn't valid"))
                };
            }
        },
        3.0f));

    return true;
}

bool FPlayerSimulateInput::RunTest(const FString& Parameters)
{
    const auto Level = LevelScope("/Game/TDBosses/Tests/TestPlayerEnemy");
    UWorld* World = GetTestGameWorld();
    if (!TestNotNull("World exists", World)) return false;

    ACharacter* Character = UGameplayStatics::GetPlayerCharacter(World, 0);
    if (!TestNotNull("Character exists", Character)) return false;

    const FString FileName = GetTestDataDir().Append(SimulateInputTest_JsonName);
    FInputData InputData;
    if (!JsonUtils::ReadInputData(FileName, InputData)) return false;
    if (!TestTrue("Input data is not empty", InputData.Bindings.Num() > 0)) return false;

    APlayerController* PlayerController = UGameplayStatics::GetPlayerController(World, 0);
    if (!TestNotNull("Player controller exists", PlayerController)) return false;

    Character->SetActorTransform(InputData.InitialTransform);
    PlayerController->SetControlRotation(InputData.InitialTransform.Rotator());

    ADD_LATENT_AUTOMATION_COMMAND(FEngineWaitLatentCommand(1.0f));

    ADD_LATENT_AUTOMATION_COMMAND(FSimulateInputActionsLatentCommand(World, Cast<UEnhancedInputComponent>(Character->InputComponent),  //
        Cast<UEnhancedPlayerInput>(PlayerController->PlayerInput), InputData.Bindings));

    ADD_LATENT_AUTOMATION_COMMAND(FEngineWaitLatentCommand(1.0f));

    return true;
}

bool FPlayerCanGetWeapon::RunTest(const FString& Parameters)
{
    const auto Level = LevelScope("/Game/TDBosses/Tests/TestPlayerEnemy");
    UWorld* World = GetTestGameWorld();
    if (!TestNotNull("World exists", World)) return false;

    ACharacter* Character = UGameplayStatics::GetPlayerCharacter(World, 0);
    if (!TestNotNull("Character exists", Character)) return false;

    UWeaponComponent* WeaponComponent = UTDHelper_Weapons::GetWeaponComponentFromActor(Character);
    if (!TestNotNull("WeaponComponent exists", WeaponComponent)) return false;

    const FString FileName = GetTestDataDir().Append(CharacterCanGetWeapon_JsonName);
    FInputData InputData;
    if (!JsonUtils::ReadInputData(FileName, InputData)) return false;
    if (!TestTrue("Input data is not empty", InputData.Bindings.Num() > 0)) return false;

    APlayerController* PlayerController = UGameplayStatics::GetPlayerController(World, 0);
    if (!TestNotNull("Player controller exists", PlayerController)) return false;

    Character->SetActorTransform(InputData.InitialTransform);
    PlayerController->SetControlRotation(InputData.InitialTransform.Rotator());

    const auto* PrevWeapon = WeaponComponent->GetCurrentWeapon();

    ADD_LATENT_AUTOMATION_COMMAND(FEngineWaitLatentCommand(1.0f));
    ADD_LATENT_AUTOMATION_COMMAND(FSimulateInputActionsLatentCommand(World, Cast<UEnhancedInputComponent>(Character->InputComponent),  //
        Cast<UEnhancedPlayerInput>(PlayerController->PlayerInput), InputData.Bindings));
    // now we must get a new weapon by input

    ADD_LATENT_AUTOMATION_COMMAND(FDelayedFunctionLatentCommand(
        [&]()
        {
            if (PrevWeapon == WeaponComponent->GetCurrentWeapon())
            {
                UE_LOG(LogTestCharacter, Error, TEXT("New weapon didn't get"));
            }
        },
        0.5f));

    return true;
}

#endif
