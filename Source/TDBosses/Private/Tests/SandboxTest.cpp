// Top Down Bosses. Project for learning
#if (WITH_DEV_AUTOMATION_TESTS || WITH_PERF_AUTOMATION_TESTS)

#include "Tests/SandboxTest.h"
#include "Tests/TestUtils.h"
#include "CoreMinimal.h"
#include "Misc/AutomationTest.h"

using namespace Test;

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FMathMaxInt, "TDBosses.Math.MaxInt",
    EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter | EAutomationTestFlags::HighPriority);

bool FMathMaxInt::RunTest(const FString& Parameters)
{
    AddInfo("Max[int] func testing");

    typedef TArray<TestPayload<TInterval<int32>, int32>> MaxIntTestPayload;
    // clang-format off
    const MaxIntTestPayload DataTest
    {
        {{15, 30}, 30}, 
        {{25, 25}, 25}, 
        {{0, 124}, 124},
        {{0, 0}, 0}, 
        {{-24, -44}, -24}, 
        {{-2, 30}, 30}, 
        {{-2, 0}, 0},
        {{-2, -2}, -2}
    };
    // clang-format on

    for (const auto Data : DataTest)
    {
        const FString TestInfo =
            FString::Printf(TEXT("test value %i and %i  |  expected %i"), Data.TestValue.Min, Data.TestValue.Max, Data.ExpectedValue);
        TestEqual(TestInfo, FMath::Max(Data.TestValue.Min, Data.TestValue.Max), Data.ExpectedValue);
    }

    return true;
}

#endif