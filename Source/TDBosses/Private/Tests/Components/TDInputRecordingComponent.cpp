// Top Down Bosses. Project for learning

#include "Tests/Components/TDInputRecordingComponent.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "Tests/Utils/JsonUtils.h"
#include "EnhancedPlayerInput.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDInputRecordingComponent, All, All);

using namespace Test;

UTDInputRecordingComponent::UTDInputRecordingComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}

void UTDInputRecordingComponent::BeginPlay()
{
    Super::BeginPlay();

    const auto* Pawn = Cast<APawn>(GetOwner());
    checkf(Pawn, TEXT("Owner isn't pawn, this component implied input component"));

    checkf(GetWorld(), TEXT("World is null"));
    checkf(Pawn->InputComponent, TEXT("Owner InputComponent is null"));

    const auto* PlayerController = Pawn->GetController<APlayerController>();
    checkf(PlayerController, TEXT("Owner PlayerController is null, this component implied it"));

    EnhancedPlayerInput = Cast<UEnhancedPlayerInput>(PlayerController->PlayerInput);
    checkf(EnhancedPlayerInput, TEXT("EnhancedPlayerInput is null, this component serialize only enhanced inputs!"));

    InputData.InitialTransform = GetOwner()->GetActorTransform();
    InputData.Bindings.Add(MakeBindingsData());
}

void UTDInputRecordingComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);
    JsonUtils::WriteInputData(GenerateFileName(), InputData);
}

void UTDInputRecordingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    InputData.Bindings.Add(MakeBindingsData());
}

FBindingsData UTDInputRecordingComponent::MakeBindingsData() const
{
    if (!EnhancedPlayerInput) return FBindingsData();

    APawn* MyPawn = Cast<APawn>(GetOwner());
    if (!MyPawn) return FBindingsData();

    UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(MyPawn->InputComponent);
    if (!EnhancedInputComponent) return FBindingsData();

    FBindingsData BindingsData;
    BindingsData.WorldTime = GetWorld()->TimeSeconds;

    for (const TUniquePtr<FEnhancedInputActionEventBinding>& ActionBinding : EnhancedInputComponent->GetActionEventBindings())
    {
        const UInputAction* Action = ActionBinding->GetAction();
        if (const FInputActionInstance* ActionData = EnhancedPlayerInput->FindActionInstanceData(Action))
        {
            BindingsData.ActionsData.Add(FActionData{ActionData->GetSourceAction()->GetFName(),  //
                ActionData->GetValue().Get<FVector>(), ActionData->GetTriggerEvent()});          //
        }
    }

    return BindingsData;
}

FString UTDInputRecordingComponent::GenerateFileName() const
{
    FString SavedDir = FPaths::ProjectSavedDir();
    const FString Date = FDateTime::Now().ToString();
    return SavedDir.Append("/Tests/").Append(FileName).Append("_").Append(Date).Append(".json");
}