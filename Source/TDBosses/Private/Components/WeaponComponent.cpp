// Top Down Bosses. Project for learning

#include "Components/WeaponComponent.h"
#include "Weapons/TDBaseWeapon.h"
#include "Engine/World.h"
#include "TDCharacter.h"
#include "Animations/AnimUtils.h"
#include "AbilitySystemGlobals.h"
#include "GAS/Abilities/Weapon/GameplayAbility_GetNextWeapon.h"
#include "GAS/Abilities/Weapon/GameplayAbility_SelectCurrentWeapon.h"
#include "AbilitySystemComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponComponent, All, All);

// @param NameFunc: name must match with weapon & weapon component declaration in headers
// (that is this func must be in base weapon header and also in this component)
#define DEFINE_WeaponFireAction(NameFunc)                                                                                                  \
    void UWeaponComponent::NameFunc()                                                                                                      \
    {                                                                                                                                      \
        if (!CanFire()) return;                                                                                                            \
        NameFunc(*CurrentWeapon);                                                                                                          \
    }                                                                                                                                      \
    void UWeaponComponent::NameFunc(ATDBaseWeapon& Weapon)                                                                                 \
    {                                                                                                                                      \
        if (Weapon.NameFunc())                                                                                                             \
        {                                                                                                                                  \
            OnCurrentWeapon_StartFire.Broadcast();                                                                                         \
        }                                                                                                                                  \
    }

constexpr static int32 WeaponNum = 2;  // max count that character may equip

UWeaponComponent::UWeaponComponent()
{
    GA_SelectCurrentWeaponClass = UGameplayAbility_SelectCurrentWeapon::StaticClass();
    GA_NextWeaponClass = UGameplayAbility_GetNextWeapon::StaticClass();

    PrimaryComponentTick.bCanEverTick = false;
}

bool UWeaponComponent::GetIsFiring() const
{
    if (!CurrentWeapon) return false;

    return CurrentWeapon->GetIsFiring();
}

void UWeaponComponent::HideAllWeapon()
{
    for (auto Weapon : Weapons)
    {
        if (Weapon) Weapon->SetVisibilityHandle(false);
    }
}

// Called when the game starts
void UWeaponComponent::BeginPlay()
{
    Super::BeginPlay();

    checkf(WeaponClasses.Num() <= WeaponNum, TEXT("Our character can hold only %i weapon items"), WeaponNum);
    InitOwnerASC();  // necessary before spawn weapons!

    SpawnWeapons();
}

void UWeaponComponent::InitOwnerASC()
{
    const AActor* Owner = GetOwner();
    checkf(Owner, TEXT("WeaponComponent: Owner is null! InitOwnerASC failed"));

    OwnerASC = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Owner);
    checkf(OwnerASC, TEXT("WeaponComponent: Owner must have ASC(AbilitySystemComponent), weapons use not their own ASC, they use owner "
                          "ASC. InitOwnerASC failed!"));

    OwnerASC->GiveAbility(FGameplayAbilitySpec(GA_SelectCurrentWeaponClass, 0, -1, this));
    OwnerASC->GiveAbility(FGameplayAbilitySpec(GA_NextWeaponClass, 0, -1, this));
}

UAbilitySystemComponent* UWeaponComponent::GetAbilitySystemComponent() const
{
    return OwnerASC;
}

void UWeaponComponent::SpawnWeapons()
{
    for (int i = 0; i < WeaponClasses.Num(); ++i)
    {
        if (!IsValid(WeaponClasses[i]))
        {
            UE_LOG(LogWeaponComponent, Warning, TEXT("The %ith class isn't valid in WeaponClasses"), i);
            continue;
        }
        SpawnWeapon(WeaponClasses[i]);
    }
    SelectWeaponByCurrentIndex();
}

// Called every frame
void UWeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

DEFINE_WeaponFireAction(DefaultFire);
DEFINE_WeaponFireAction(SpecialFire);

void UWeaponComponent::StopFire()
{
    if (!CurrentWeapon) return;
    StopFire(*CurrentWeapon);
}
void UWeaponComponent::StopFire(ATDBaseWeapon& Weapon)
{
    Weapon.StopFire();
}

void UWeaponComponent::TryReload()
{
    if (!CurrentWeapon) return;
    CurrentWeapon->TryReload();
}

void UWeaponComponent::NextWeapon()
{
    if (OwnerASC) OwnerASC->TryActivateAbilityByClass(GA_NextWeaponClass, true);
}

bool UWeaponComponent::TryToAddWeapon(UClass* WeaponClass)
{
    if (WeaponClasses.Num() >= WeaponNum)
    {
        UE_LOG(LogWeaponComponent, Warning, TEXT("Our Character can hold only %i weapon items "), WeaponNum);
        return false;
    }

    auto Weapon = SpawnWeapon(WeaponClass);
    if (!Weapon) return false;

    WeaponClasses.Add(Weapon->GetClass());

    return true;
}

ATDBaseWeapon* UWeaponComponent::GetCurrentWeapon() const
{
    return CurrentWeapon;
}

ATDBaseWeapon* UWeaponComponent::SpawnWeapon(UClass* WeaponClass)
{
    if (!GetWorld()) return nullptr;

    const auto Weapon = GetWorld()->SpawnActor<ATDBaseWeapon>(WeaponClass);
    if (!Weapon) return nullptr;

    AttachWeaponToCharacter(Weapon, EquipAttachSocketName.SocketName);

    Weapons.Add(Weapon);

    BindWeaponNotifies(Weapon);

    return Weapon;
}

void UWeaponComponent::BindWeaponNotifies(ATDBaseWeapon* Weapon)
{
    if (!Weapon) return;
    const auto& WeaponAnimationData = Weapon->GetWeaponAnimationData();

    BindTDAnimNotify_UObject(UTDWeaponTakenAnimNotify, WeaponAnimationData.EquipMontage, &ThisClass::OnWeaponTaken);
}

void UWeaponComponent::AttachWeaponToCharacter(ATDBaseWeapon* Weapon, FName Socket)
{
    auto Character = Cast<ACharacter>(GetOwner());
    if (!Character) return;

    FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
    Weapon->AttachToComponent(Character->GetMesh(), AttachmentRules, Socket);
    Weapon->SetOwner(Character);
    Weapon->SetInstigator(Character);

    Weapon->SetVisibilityHandle(false);  // we play with visibility when equip/uneqiup so init state is invisible

    Weapon->OnAttachedToOwner();
}

void UWeaponComponent::SelectWeaponByCurrentIndex()
{
    if (OwnerASC) OwnerASC->TryActivateAbilityByClass(GA_SelectCurrentWeaponClass, true);
    // GameplayAbility play anim montage so then should executes OnWeaponTaken
}

void UWeaponComponent::OnWeaponTaken(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,  //
    const FAnimNotifyEventReference& EventReference)                                                  //
{
    /* old weapon that already in hands */
    if (CurrentWeapon)
    {
        CurrentWeapon->SetVisibilityHandle(false);
        CurrentWeapon->OnUnequipped();
    }
    /* new weapon */
    CurrentWeapon = Weapons[CurrentWeaponIndex];

    CurrentWeapon->SetVisibilityHandle(true);
    CurrentWeapon->OnEquipped();
}

bool UWeaponComponent::CanFire() const
{
    return CurrentWeapon != nullptr && !GetIsFiring();
}
