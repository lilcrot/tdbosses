// Top Down Bosses. Project for learning

#include "Components/HealthComponent.h"
#include "GameplayCueFunctionLibrary.h"
#include "PlayerCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthComponent, All, All);

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
    Super::BeginPlay();
    CurrentHealth = MaxHealth;
    checkf(MaxHealth > 0, TEXT("DefaultHealth must be more than zero"));

    AActor* MyOwner = GetOwner();
    if (MyOwner)
    {
        MyOwner->OnTakeAnyDamage.AddDynamic(this, &ThisClass::OnTakeAnyDamage);
        MyOwner->OnTakePointDamage.AddDynamic(this, &ThisClass::OnTakePointDamage);
        MyOwner->OnTakeRadialDamage.AddDynamic(this, &ThisClass::OnTakeRadialDamage);
    }
}

void UHealthComponent::SetHealth(int32 NewHealth)
{
    CurrentHealth = FMath::Clamp(NewHealth, 0.0f, MaxHealth);
    OnHealthChanged(NewHealth);
}

void UHealthComponent::OnHealthChanged(int32 NewHealth)
{
    // broadcast delegates
    NewHealth <= 0 ? OnDeath.Broadcast() : OnHealthChangedDelegate.Broadcast(CurrentHealth);
}

void UHealthComponent::SetInvulnerability()
{
    if (UWorld* World = GetWorld())
    {
        bInvulnerability = true;
        World->GetTimerManager().SetTimer(InvulnerabilityTimer, this, &UHealthComponent::ResetInvulnerability, 1.0f,  //
            false, TimeOfInvulnerability);                                                                            //
    }
}
void UHealthComponent::ResetInvulnerability()
{
    if (UWorld* World = GetWorld())
    {
        bInvulnerability = false;
        World->GetTimerManager().ClearTimer(InvulnerabilityTimer);
    }
}

void UHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,  //
    AController* InstigatedBy, AActor* DamageCauser)                                                       //
{
    if (IsInvulnerable() || Damage <= 0.0f) return;
    if (bCanInvulnerable) SetInvulnerability();

    SetHealth(CurrentHealth - Damage);
    if (CurrentHealth <= 0) OnDeath.Broadcast();

    PlayCameraShake();
}

void UHealthComponent::OnTakePointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation,
    UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser)
{
    if (IsInvulnerable() || Damage <= 0.0f) return;
    OnTakenPointHit.Broadcast(DamagedActor, Damage, InstigatedBy, HitLocation, FHitComponent,  //
        BoneName, ShotFromDirection, DamageType, DamageCauser);                                //

    /* Play hit react */
    {
        FGameplayCueParameters Params;
        Params.Normal = FVector_NetQuantizeNormal(ShotFromDirection);
        UGameplayCueFunctionLibrary::ExecuteGameplayCueOnActor(GetOwner(), HitReactGameplayCueTag, Params);
    }
}

void UHealthComponent::OnTakeRadialDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, FVector Origin,
    FHitResult HitInfo, AController* InstigatedBy, AActor* DamageCauser)
{
    if (IsInvulnerable() || Damage <= 0.0f) return;
    OnTakenRadialHit.Broadcast(DamagedActor, Damage, DamageType, Origin, HitInfo, InstigatedBy, DamageCauser);

    /* Play hit react */
    {
        FGameplayCueParameters Params;
        Params.Normal = FVector_NetQuantizeNormal(Origin);
        UGameplayCueFunctionLibrary::ExecuteGameplayCueOnActor(GetOwner(), HitReactGameplayCueTag, Params);
    }
}

void UHealthComponent::PlayCameraShake()
{
    if (IsDead()) return;

    const auto Player = Cast<APlayerCharacter>(GetOwner());
    if (!Player) return;
    const auto Controller = Cast<APlayerController>(Player->GetController());
    if (!Controller) return;

    Controller->PlayerCameraManager->StartCameraShake(CameraShake);
}
