// Top Down Bosses. Project for learning

#include "GAS/TDAbilitySystemComponent.h"
#include "GAS/Abilities/TDGameplayAbility.h"
#include "InputAction.h"
#include "EnhancedInputComponent.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDAbilitySystemComponent, All, All);

namespace
{
constexpr int32 InvalidInputID = 0;
int32 IncrementingInputID = InvalidInputID;

static int32 GetNextInputID()
{
    return ++IncrementingInputID;
}
}  // namespace

void UTDAbilitySystemComponent::BeginPlay()
{
    Super::BeginPlay();

    const AActor* MyOwner = GetOwner();
    if (IsValid(MyOwner) && MyOwner->InputComponent)
    {
        InputComponent = Cast<UEnhancedInputComponent>(MyOwner->InputComponent);
        if (!IsValid(InputComponent))
        {
            UE_LOG(LogTDAbilitySystemComponent, Error,
                TEXT("InputComponent(UEnhancedInputComponent) is null, all ability bindings will be failed!"));
        }
    }
}

void UTDAbilitySystemComponent::NotifyAbilityActivated(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability)
{
    Super::NotifyAbilityActivated(Handle, Ability);

    if (UTDGameplayAbility* TDAbility = Cast<UTDGameplayAbility>(Ability))
    {
        AddAbilityToActivationGroup(TDAbility->GetActivationGroup(), TDAbility);
    }
}

void UTDAbilitySystemComponent::NotifyAbilityEnded(FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability, bool bWasCancelled)
{
    Super::NotifyAbilityEnded(Handle, Ability, bWasCancelled);

    if (UTDGameplayAbility* TDAbility = Cast<UTDGameplayAbility>(Ability))
    {
        RemoveAbilityFromActivationGroup(TDAbility->GetActivationGroup(), TDAbility);
    }
}

//---------------------------
//  Ability ActivatingPolicy
//---------------------------

bool UTDAbilitySystemComponent::IsActivationGroupBlocked(EAbilityActivationGroup Group) const
{
    bool bBlocked = false;

    switch (Group)
    {
        case EAbilityActivationGroup::Independent:
        {
            // Independent abilities are never blocked.
            bBlocked = false;
            break;
        }

        case EAbilityActivationGroup::Exclusive_Replaceable:
        case EAbilityActivationGroup::Exclusive_Blocking:
            // Exclusive abilities can activate if nothing is blocking.
            bBlocked = (GetActivationCountsByGroup(EAbilityActivationGroup::Exclusive_Blocking) > 0);
            break;

        default:
        {
            checkf(false, TEXT("IsActivationGroupBlocked: Invalid ActivationGroup [%d]\n"), (uint8)Group);
            break;
        }
    }
    return bBlocked;
}

void UTDAbilitySystemComponent::AddAbilityToActivationGroup(EAbilityActivationGroup Group, UTDGameplayAbility* TDAbility)
{
    check(TDAbility);
    check(GetActivationCountsByGroup(Group) < INT32_MAX);

    ActivationGroupCounts[(uint8)Group]++;

    const bool bReplicateCancelAbility = false;

    switch (Group)
    {
        case EAbilityActivationGroup::Independent:
        {
            // Independent abilities do not cancel any other abilities.
            break;
        }

        case EAbilityActivationGroup::Exclusive_Replaceable:
        case EAbilityActivationGroup::Exclusive_Blocking:
            CancelActivationGroupAbilities(EAbilityActivationGroup::Exclusive_Replaceable, TDAbility, bReplicateCancelAbility);
            break;

        default: checkf(false, TEXT("AddAbilityToActivationGroup: Invalid ActivationGroup [%d]\n"), (uint8)Group); break;
    }

    const int32 ExclusiveCount = GetActivationCountsByGroup(EAbilityActivationGroup::Exclusive_Replaceable) +
                                 GetActivationCountsByGroup(EAbilityActivationGroup::Exclusive_Blocking);
    if (!ensure(ExclusiveCount <= 1))
    {
        UE_LOG(LogTDAbilitySystemComponent, Error, TEXT("AddAbilityToActivationGroup: Multiple exclusive abilities are running."));
    }
}

void UTDAbilitySystemComponent::RemoveAbilityFromActivationGroup(EAbilityActivationGroup Group, UTDGameplayAbility* TDAbility)
{
    check(TDAbility);
    check(GetActivationCountsByGroup(Group) > 0);

    ActivationGroupCounts[(uint8)Group]--;
}

void UTDAbilitySystemComponent::CancelActivationGroupAbilities(EAbilityActivationGroup Group, UTDGameplayAbility* IgnoreAbility,  //
    bool bReplicateCancelAbility)                                                                                                 //
{
    auto ShouldCancelFunc = [this, Group, IgnoreAbility](const UTDGameplayAbility* TDAbility, FGameplayAbilitySpecHandle Handle)
    { return ((TDAbility->GetActivationGroup() == Group) && (TDAbility != IgnoreAbility)); };

    CancelAbilitiesByFunc(ShouldCancelFunc, bReplicateCancelAbility);
}

void UTDAbilitySystemComponent::CancelAbilitiesByFunc(TShouldCancelAbilityFunc ShouldCancelFunc, bool bReplicateCancelAbility)
{
    ABILITYLIST_SCOPE_LOCK();
    for (const FGameplayAbilitySpec& AbilitySpec : ActivatableAbilities.Items)
    {
        if (!AbilitySpec.IsActive())
        {
            continue;
        }

        UTDGameplayAbility* TDAbilityCDO = CastChecked<UTDGameplayAbility>(AbilitySpec.Ability);

        if (TDAbilityCDO->GetInstancingPolicy() != EGameplayAbilityInstancingPolicy::NonInstanced)
        {
            // Cancel all the spawned instances, not the CDO.
            TArray<UGameplayAbility*> Instances = AbilitySpec.GetAbilityInstances();
            for (UGameplayAbility* AbilityInstance : Instances)
            {
                UTDGameplayAbility* TDAbilityInstance = CastChecked<UTDGameplayAbility>(AbilityInstance);

                if (ShouldCancelFunc(TDAbilityInstance, AbilitySpec.Handle))
                {
                    if (TDAbilityInstance->CanBeCanceled())
                    {
                        TDAbilityInstance->CancelAbility(AbilitySpec.Handle, AbilityActorInfo.Get(),
                            TDAbilityInstance->GetCurrentActivationInfo(), bReplicateCancelAbility);
                    }
                    else
                    {
                        UE_LOG(LogTDAbilitySystemComponent, Error,
                            TEXT("CancelAbilitiesByFunc: Can't cancel ability [%s] because CanBeCanceled is false."),
                            *TDAbilityInstance->GetName());
                    }
                }
            }
        }
        else
        {
            // Cancel the non-instanced ability CDO.
            if (ShouldCancelFunc(TDAbilityCDO, AbilitySpec.Handle))
            {
                // Non-instanced abilities can always be canceled.
                check(TDAbilityCDO->CanBeCanceled());
                TDAbilityCDO->CancelAbility(
                    AbilitySpec.Handle, AbilityActorInfo.Get(), FGameplayAbilityActivationInfo(), bReplicateCancelAbility);
            }
        }
    }
}

//------------------------
// Enhanced Input Bindings
//------------------------

void UTDAbilitySystemComponent::SetInputBinding(UInputAction* InputAction, const FGameplayAbilitySpecHandle& AbilityHandle)
{
    FGameplayAbilitySpec* AbilityToBind = FindAbilitySpecFromHandle(AbilityHandle);
    if (!AbilityToBind)
    {
        UE_LOG(LogTDAbilitySystemComponent, Warning, TEXT("AbilityToBind is null. SetInputBinding failed!"));
        return;
    }

    /* Set input id to new ability. If aready have then delete old bind and rebind to new */
    FAbilityInputBinding* AbilityInputBinding = MappedAbilities.Find(InputAction);
    {
        if (AbilityInputBinding)
        {
            FGameplayAbilitySpec* OldBoundAbility = FindAbilitySpecFromHandle(AbilityInputBinding->BoundAbility);
            if (OldBoundAbility && OldBoundAbility->InputID == AbilityInputBinding->InputID)
            {
                ClearAbilityInputBinding(OldBoundAbility->Handle);
            }
        }
        else
        {
            AbilityInputBinding = &MappedAbilities.Add(InputAction);
            AbilityInputBinding->InputID = GetNextInputID();
        }
    }

    AbilityToBind->InputID = AbilityInputBinding->InputID;

    AbilityInputBinding->BoundAbility = AbilityHandle;
    TryBindAbilityInput(InputAction, *AbilityInputBinding);
}

void UTDAbilitySystemComponent::ClearAbilityInputBinding(const FGameplayAbilitySpecHandle& AbilityHandle)
{
    FGameplayAbilitySpec* FoundAbility = FindAbilitySpecFromHandle(AbilityHandle);
    if (!FoundAbility) return;

    // Find the mapping for this ability and exit from func if not found
    auto MappedIterator = MappedAbilities.CreateIterator();
    {
        while (MappedIterator)
        {
            if (MappedIterator.Value().InputID == FoundAbility->InputID) break;
            ++MappedIterator;
        }
        if (!MappedIterator) return;
    }

    ClearAbilityBindingFromInputAction(MappedIterator.Key());
}

void UTDAbilitySystemComponent::ClearAbilityBindingFromInputAction(const UInputAction* InputAction)
{
    if (const FAbilityInputBinding* Bindings = MappedAbilities.Find(InputAction))
    {
        if (InputComponent)
        {
            InputComponent->RemoveBindingByHandle(Bindings->OnPressedHandle);
            InputComponent->RemoveBindingByHandle(Bindings->OnReleasedHandle);
        }

        const FGameplayAbilitySpecHandle& AbilityHandle = Bindings->BoundAbility;
        FGameplayAbilitySpec* AbilitySpec = FindAbilitySpecFromHandle(AbilityHandle);
        if (AbilitySpec && AbilitySpec->InputID == Bindings->InputID)
        {
            AbilitySpec->InputID = InvalidInputID;
        }

        MappedAbilities.Remove(InputAction);
    }
}

void UTDAbilitySystemComponent::TryBindAbilityInput(UInputAction* InputAction, FAbilityInputBinding& AbilityInputBinding)
{
    if (!InputComponent)
    {
        UE_LOG(LogTDAbilitySystemComponent, Warning, TEXT("TryBindAbilityInput: Input component is null, binding failed"));
        return;
    }

    // Pressed event
    if (AbilityInputBinding.OnPressedHandle == 0)
    {
        AbilityInputBinding.OnPressedHandle =
            InputComponent->BindAction(InputAction, ETriggerEvent::Started, this, &ThisClass::OnAbilityInputPressed, InputAction)
                .GetHandle();
    }

    // Released event
    if (AbilityInputBinding.OnReleasedHandle == 0)
    {
        AbilityInputBinding.OnReleasedHandle =
            InputComponent->BindAction(InputAction, ETriggerEvent::Completed, this, &ThisClass::OnAbilityInputReleased, InputAction)
                .GetHandle();
    }
}

void UTDAbilitySystemComponent::OnAbilityInputPressed(UInputAction* InputAction)
{
    const FAbilityInputBinding* FoundBinding = MappedAbilities.Find(InputAction);
    if (FoundBinding && ensure(FoundBinding->InputID != InvalidInputID))
    {
        AbilityLocalInputPressed(FoundBinding->InputID);
    }
}
void UTDAbilitySystemComponent::OnAbilityInputReleased(UInputAction* InputAction)
{
    const FAbilityInputBinding* FoundBinding = MappedAbilities.Find(InputAction);
    if (FoundBinding && ensure(FoundBinding->InputID != InvalidInputID))
    {
        AbilityLocalInputReleased(FoundBinding->InputID);
    }
}
