// Top Down Bosses. Project for learning

#include "GAS/Abilities/Pawn/GameplayAbility_Dash.h"
#include "Abilities/Tasks/AbilityTask_ApplyRootMotionConstantForce.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "GameFramework/Character.h"

UGameplayAbility_Dash::UGameplayAbility_Dash()
{
    ActivationGroup = EAbilityActivationGroup::Exclusive_Blocking;

    DashStrength = 1800.0f;
    RootMotionDuration = 0.25f;
    AbilityDuration = 0.50f;
}

void UGameplayAbility_Dash::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

    const APawn* MyPawn = Cast<APawn>(ActorInfo->OwnerActor.Get());
    if (!MyPawn)
    {
        ForcedEndAbilityWithWarningLog(Handle, ActorInfo, ActivationInfo, false, "Pawn is null");
        return;
    }
    CommitAbility(Handle, ActorInfo, ActivationInfo);

    AController* PawnController = MyPawn->GetController();
    if (PawnController) PawnController->SetIgnoreMoveInput(true);

    /* Play dash montage and wait */
    {
        UAbilityTask_PlayMontageAndWait* AT_Animation =
            UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, NAME_None, DashMontage, 1.0f, NAME_None, false);
        AT_Animation->OnCancelled.AddDynamic(this, &ThisClass::ForcedEndAbility);
        AT_Animation->OnInterrupted.AddDynamic(this, &ThisClass::ForcedEndAbility);
        AT_Animation->ReadyForActivation();
    }

    /* Apply root motion constant force to dash montage */
    {
        const FVector Direction = MyPawn->GetActorForwardVector();
        UAbilityTask_ApplyRootMotionConstantForce* AT_RootMotion =
            UAbilityTask_ApplyRootMotionConstantForce::ApplyRootMotionConstantForce(this, NAME_None, Direction, DashStrength,
                RootMotionDuration, true, nullptr, ERootMotionFinishVelocityMode::ClampVelocity, FVector::ZeroVector, 1000.0f, false);

        AT_RootMotion->OnFinish.AddDynamic(this, &ThisClass::OnApplyRootMotionFinished);
        AT_RootMotion->ReadyForActivation();
    }

    DashVisual();
}

void UGameplayAbility_Dash::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
    if (const APawn* MyPawn = Cast<APawn>(ActorInfo->OwnerActor.Get()))
    {
        AController* PawnController = MyPawn->GetController();
        if (PawnController) PawnController->ResetIgnoreMoveInput();
    }

    Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UGameplayAbility_Dash::OnApplyRootMotionFinished()
{
    UWorld* World = GetWorld();
    if (!World)
    {
        ForcedEndAbilityWithWarningLog(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, "World is null");
        return;
    }

    /* It looks more beautiful, character landing with small acceleration */
    if (ACharacter* AvatarCharacter = Cast<ACharacter>(CurrentActorInfo->AvatarActor.Get()))
    {
        AvatarCharacter->LaunchCharacter(FVector(10.0, 0.0f, 0.0f), false, false);
    }

    /* We can't immidently activate other ability */
    FTimerHandle DelayTimer;
    World->GetTimerManager().SetTimer(
        DelayTimer, [this]() { EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false); },
        AbilityDuration - RootMotionDuration, false);
}

void UGameplayAbility_Dash::ForcedEndAbility()
{
    EndAbilityWithCurrentInfo(true, true);
}

// settuped in bp
void UGameplayAbility_Dash::DashVisual_Implementation() {}
