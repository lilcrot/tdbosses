// Top Down Bosses. Project for learning

#include "GAS/Abilities/TDGameplayAbility.h"
#include "GAS/TDAbilitySystemComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDAbility, All, All);

UTDGameplayAbility::UTDGameplayAbility() {}

void UTDGameplayAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    if (bHasBlueprintActivate)
    {
        // A Blueprinted ActivateAbility function must call CommitAbility somewhere in its execution chain.
        K2_ActivateAbility();
    }
    else if (bHasBlueprintActivateFromEvent)
    {
        if (TriggerEventData)
        {
            // A Blueprinted ActivateAbility function must call CommitAbility somewhere in its execution chain.
            K2_ActivateAbilityFromEvent(*TriggerEventData);
        }
        else
        {
            ForcedEndAbilityWithWarningLog(Handle, ActorInfo, ActivationInfo, false,                                                     //
                "Ability expects event data but none is being supplied. Use Activate Ability instead of Activate Ability From Event.");  //
        }
    }
}

bool UTDGameplayAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const
{
    if (!Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
    {
        return false;
    }

    auto* OwnerASC = Cast<UTDAbilitySystemComponent>(ActorInfo->AbilitySystemComponent.Get());
    if (!IsValid(OwnerASC)) return false;

    return !OwnerASC->IsActivationGroupBlocked(ActivationGroup);
}

void UTDGameplayAbility::ForcedEndAbilityWithWarningLog(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, const FString& Msg)
{
    FString MsgWithClassName = *Msg;
    if (const UClass* AbilityClass = GetClass()) MsgWithClassName = AbilityClass->GetName() + ": " + *Msg;  // Format[ClassName: Msg]
    UE_LOG(LogTDAbility, Warning, TEXT("%s"), *MsgWithClassName);
    const bool bWasCancelled = true;  // becuse FORCED end ability
    EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UTDGameplayAbility::ForcedCurEndAbilityWithWarningLog(bool bReplicateEndAbility, const FString& Msg)
{
    ForcedEndAbilityWithWarningLog(GetCurrentAbilitySpecHandle(), GetCurrentActorInfo(),  //
        GetCurrentActivationInfo(), bReplicateEndAbility, Msg);                           //
}

void UTDGameplayAbility::EndAbilityWithCurrentInfo(bool bReplicateEndAbility, bool bWasCancelled)
{
    EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, bReplicateEndAbility, bWasCancelled);
}
