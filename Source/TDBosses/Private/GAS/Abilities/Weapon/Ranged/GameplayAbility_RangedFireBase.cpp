#include "GAS/Abilities/Weapon/Ranged/GameplayAbility_RangedFireBase.h"
#include "Weapons/Ranged/RangedWeapon.h"

UGameplayAbility_RangedFireBase::UGameplayAbility_RangedFireBase()
{
    ActivationGroup = EAbilityActivationGroup::Exclusive_Blocking;
}

ARangedWeapon* UGameplayAbility_RangedFireBase::GetRangedWeapon()
{
    return Cast<ARangedWeapon>(GetCurrentWeapon());
}

void UGameplayAbility_RangedFireBase::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
}

void UGameplayAbility_RangedFireBase::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
    Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

bool UGameplayAbility_RangedFireBase::GetTraceData(FVector& TraceStart, FVector& TraceEnd,  //
    FVector& ShotDirection, FCollisionQueryParams& CollisionQueryParams) const              //
{
    auto* Weapon = GetCurrentWeapon();
    if (!Weapon) return false;

    const FWeaponFireData& WeaponFireData = Weapon->GetWeaponFireData();
    const FTransform& MuzzleSocketTransform = Weapon->GetMuzzleSocketTransform();

    ShotDirection = MuzzleSocketTransform.GetRotation().Vector();  // Get Normalized direction vector
    TraceStart = MuzzleSocketTransform.GetLocation();
    TraceEnd = TraceStart + ShotDirection * WeaponFireData.ShotRange;
    TraceEnd.Z = MuzzleSocketTransform.GetLocation().Z;  // freeze Z axe

    CollisionQueryParams.AddIgnoredActor(Weapon->GetOwner());
    CollisionQueryParams.AddIgnoredActor(Weapon);
    CollisionQueryParams.bTraceComplex = true;
    CollisionQueryParams.bReturnPhysicalMaterial = true;

    return true;
}

bool UGameplayAbility_RangedFireBase::CanShot() const
{
    const auto* Weapon = GetCurrentWeapon();
    if (!Weapon) return false;
    const auto& AmmoData = Weapon->GetAmmoData();
    return AmmoData.bInfinite || AmmoData.GetCurrentBullets() > 0;
}
