#include "GAS/Abilities/Weapon/Ranged/RangedFireAbility_QuickFire.h"
#include "TDCoreTypes.h"
#include "Weapons/TDBaseWeapon.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"

DEFINE_LOG_CATEGORY_STATIC(LogQuickFireAbility, All, All);

URangedFireAbility_QuickFire::URangedFireAbility_QuickFire() {}

void URangedFireAbility_QuickFire::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    if (!GetWorld())
    {
        EndAbilityWithCurrentInfo();
        return;
    }
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

    const auto* Weapon = GetCurrentWeapon();
    if (!Weapon)
    {
        ForcedEndAbilityWithWarningLog(Handle, ActorInfo, ActivationInfo, true, "Weapon is null");
        return;
    }
    CommitAbility(Handle, ActorInfo, ActivationInfo);

    const FWeaponFireData& WeaponFireData = Weapon->GetWeaponFireData();
    const float ActualShotRate = PlusShotRate + WeaponFireData.ShootRate;

    GetWorld()->GetTimerManager().SetTimer(FireHandle, this, &ThisClass::QuickFire,  //
        ActualShotRate, WeaponFireData.bAutoFire, -1.0f);                            //

    UAbilityTask_WaitGameplayEvent* WaitStopFire =
        UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, Weapon->GetStopFireTag(), nullptr, true, true);

    WaitStopFire->EventReceived.AddDynamic(this, &ThisClass::OnStopFireTagRecieved);
    WaitStopFire->ReadyForActivation();
}

void URangedFireAbility_QuickFire::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
    if (!GetWorld()) return;
    GetWorld()->GetTimerManager().ClearTimer(FireHandle);

    Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void URangedFireAbility_QuickFire::OnStopFireTagRecieved(FGameplayEventData Payload)
{
    EndAbilityWithCurrentInfo(true, false);
}

void URangedFireAbility_QuickFire::QuickFire()
{
    for (int32 i = 0; i < DirectionDeggreeDeltas.Num(); i++)
    {
        if (!DirectionDeggreeDeltas.IsValidIndex(i))
        {
            UE_LOG(LogQuickFireAbility, Warning, TEXT("The %i-th elem in DirectionDeggreeDeltas isn't valid"), i);
            continue;
        }
        ShotWithDirectionDelta(DirectionDeggreeDeltas[i]);
    }
}

void URangedFireAbility_QuickFire::ShotWithDirectionDelta(const float DeltaDirection)
{
    if (!CanShot() || !GetWorld())
    {
        EndAbilityWithCurrentInfo();  // stop fire
        return;
    }
    auto* Weapon = GetCurrentWeapon();
    if (!Weapon)
    {
        EndAbilityWithCurrentInfo();  // stop fire
        return;
    }

    FVector TraceStart, TraceEnd, ShotDirection;
    FCollisionQueryParams CollisionQueryParams;
    if (!GetQuickFireTraceData(TraceStart, TraceEnd, ShotDirection, CollisionQueryParams, DeltaDirection))
    {
        EndAbilityWithCurrentInfo();  // stop fire
        return;
    }

    FHitResult Hit;
    GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, TraceChannel_Weapon, CollisionQueryParams);

    if (Hit.bBlockingHit)
    {
        MakePointDamage(Hit, ShotDirection, Weapon->GetWeaponFireData().DamageAmount, Weapon->GetOwner());
    }

    DrawWeaponDebug(Hit, TraceStart, TraceEnd);
    DecreaseAmmo(*Weapon);
    PlayEffects(Hit, DeltaDirection);
}

bool URangedFireAbility_QuickFire::GetQuickFireTraceData(FVector& TraceStart, FVector& TraceEnd, FVector& ShotDirection,  //
    FCollisionQueryParams& CollisionQueryParams, const float DeltaDirection) const                                        //
{
    auto* Weapon = GetCurrentWeapon();
    if (!Weapon) return false;

    const FWeaponFireData& WeaponFireData = Weapon->GetWeaponFireData();
    const USkeletalMeshComponent* WeaponMeshComponent = Weapon->GetMesh();

    const FTransform SocketTransform = WeaponMeshComponent->GetSocketTransform(WeaponFireData.MuzzleSocketName);
    const auto SocketZ = SocketTransform.GetLocation().Z;

    TraceStart = SocketTransform.GetLocation();

    const FRotator RotatedRotation = SocketTransform.GetRotation().Rotator() + FRotator(0.0f, DeltaDirection, 0.0f);
    ShotDirection = RotatedRotation.Vector();

    TraceEnd = TraceStart + (ShotDirection * WeaponFireData.ShotRange);
    TraceEnd.Z = SocketZ;  // freeze Z-axis, shot only soket Zline

    CollisionQueryParams.AddIgnoredActor(Weapon->GetOwner());
    CollisionQueryParams.AddIgnoredActor(Weapon);
    CollisionQueryParams.bTraceComplex = true;
    CollisionQueryParams.bReturnPhysicalMaterial = true;

    return true;
}

void URangedFireAbility_QuickFire::PlayEffects_Implementation(const FHitResult& Hit, const float DeltaDirectionDegree) {}