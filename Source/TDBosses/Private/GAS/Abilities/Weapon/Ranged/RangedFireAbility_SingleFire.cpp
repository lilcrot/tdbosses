#include "GAS/Abilities/Weapon/Ranged/RangedFireAbility_SingleFire.h"
#include "TDCoreTypes.h"
#include "Weapons/TDBaseWeapon.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"

URangedFireAbility_SingleFire::URangedFireAbility_SingleFire() {}

void URangedFireAbility_SingleFire::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    if (!GetWorld())
    {
        EndAbilityWithCurrentInfo();
        return;
    }
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

    const auto* Weapon = GetCurrentWeapon();
    if (!Weapon)
    {
        ForcedEndAbilityWithWarningLog(Handle, ActorInfo, ActivationInfo, true, "Weapon is null");
        return;
    }
    CommitAbility(Handle, ActorInfo, ActivationInfo);

    const FWeaponFireData& WeaponFireData = Weapon->GetWeaponFireData();
    GetWorld()->GetTimerManager().SetTimer(FireHandle, this, &ThisClass::Fire,  //
        WeaponFireData.ShootRate, WeaponFireData.bAutoFire, -1.0f);             //

    UAbilityTask_WaitGameplayEvent* WaitStopFire =
        UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this, Weapon->GetStopFireTag(), nullptr, true, true);

    WaitStopFire->EventReceived.AddDynamic(this, &ThisClass::OnStopFireTagRecieved);
    WaitStopFire->ReadyForActivation();
}

void URangedFireAbility_SingleFire::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
    if (!GetWorld()) return;
    GetWorld()->GetTimerManager().ClearTimer(FireHandle);

    Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void URangedFireAbility_SingleFire::OnStopFireTagRecieved(FGameplayEventData Payload)
{
    EndAbilityWithCurrentInfo(true, false);
}

void URangedFireAbility_SingleFire::Fire()
{
    if (!CanShot() || !GetWorld())
    {
        EndAbilityWithCurrentInfo();  // stop fire
        return;
    }
    auto* Weapon = GetCurrentWeapon();
    if (!Weapon)
    {
        EndAbilityWithCurrentInfo();  // stop fire
        return;
    }

    FVector TraceStart, TraceEnd, ShotDirection;
    FCollisionQueryParams CollisionQueryParams;
    if (!GetTraceData(TraceStart, TraceEnd, ShotDirection, CollisionQueryParams))
    {
        EndAbilityWithCurrentInfo();  // stop fire
        return;
    }

    FHitResult Hit;
    GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, TraceChannel_Weapon, CollisionQueryParams);

    if (Hit.bBlockingHit)
    {
        MakePointDamage(Hit, ShotDirection, Weapon->GetWeaponFireData().DamageAmount, Weapon->GetOwner());
    }

    DrawWeaponDebug(Hit, TraceStart, TraceEnd);
    DecreaseAmmo(*Weapon);
    PlayEffects(Hit);
}

void URangedFireAbility_SingleFire::PlayEffects_Implementation(const FHitResult& Hit) {}
