#include "GAS/Abilities/Weapon/Melee/MeleeFireAbility_JumpRadial.h"
#include "Weapons/TDBaseWeapon.h"
#include "Weapons/Melee/MeleeWeapon.h"
#include "Animations/AnimUtils.h"

UMeleeFireAbility_JumpRadial::UMeleeFireAbility_JumpRadial()
{
    PlusDamageAmount = 1.0f;
    DamageRadius = 250.0f;
}

void UMeleeFireAbility_JumpRadial::OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
    Super::OnGiveAbility(ActorInfo, Spec);
    BindTDAnimNotify_Dynamic(UTDJumpRadialDamageAnimNotify, GetCharacterAttackMontage(), &ThisClass::OnNotifyJumpRadialDamage);
}

void UMeleeFireAbility_JumpRadial::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

    CommitAbility(Handle, ActorInfo, ActivationInfo);
}

void UMeleeFireAbility_JumpRadial::OnNotifyStartAttack(
    USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    Super::OnNotifyStartAttack(MeshComp, Animation, EventReference);
    // nothing, only apply radial damage on NotifyJumpRadialDamage
}

void UMeleeFireAbility_JumpRadial::OnNotifyEndAttack(
    USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    Super::OnNotifyEndAttack(MeshComp, Animation, EventReference);
}

void UMeleeFireAbility_JumpRadial::OnNotifyJumpRadialDamage(
    USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    const auto* Weapon = GetCurrentWeapon();
    if (!Weapon) return;
    const auto WeaponFireData = Weapon->GetWeaponFireData();

    AActor* Instigator = Weapon->GetInstigator();
    const FVector Origin = GetStartPointOfBlade();

    const float ActualDamage = PlusDamageAmount + WeaponFireData.DamageAmount;
    ApplyRadialDamage(Origin, DamageRadius, bApplyFullRadialDamage, Instigator, ActualDamage);

    PlayEffects(Origin);
}

void UMeleeFireAbility_JumpRadial::PlayEffects_Implementation(const FVector& Origin) {}
