// Top Down Bosses. Project for learning

#include "GAS/Abilities/Weapon/Melee/MeleeFireAbility_SingleAttack.h"
#include "Weapons/TDBaseWeapon.h"
#include "Weapons/Melee/MeleeWeapon.h"

UMeleeFireAbility_SingleAttack::UMeleeFireAbility_SingleAttack() {}

void UMeleeFireAbility_SingleAttack::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

    CommitAbility(Handle, ActorInfo, ActivationInfo);
}

void UMeleeFireAbility_SingleAttack::OnNotifyStartAttack(
    USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    Super::OnNotifyStartAttack(MeshComp, Animation, EventReference);

    if (UWorld* World = GetWorld())
    {
        constexpr float Rate = 0.01f;
        World->GetTimerManager().SetTimer(LineTraceTimer, this, &ThisClass::LoopMeleeSphereTrace, Rate, true, 0.0f);
    }
}

void UMeleeFireAbility_SingleAttack::OnNotifyEndAttack(
    USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    Super::OnNotifyEndAttack(MeshComp, Animation, EventReference);

    if (UWorld* World = GetWorld())
    {
        World->GetTimerManager().ClearTimer(LineTraceTimer);
    }
}