#include "GAS/Abilities/Weapon/Melee/GameplayAbility_MeleeFireBase.h"
#include "Weapons/Melee/MeleeWeapon.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Animations/AnimUtils.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"

DEFINE_LOG_CATEGORY_STATIC(LogMeleeAbility_FireBase, All, All);

// @param AnimNotifyNameClass: without StaticClass::(), only name, e.g TDAnimNotify
// @param CallbackFunction: format are OwnerFuncClassName::Func, e.g &ThisClass::OnAnimNotify
#define BindTrailAnimNotify_UObject(AnimNotifyNameClass, Animation, CallbackFunction)                                                      \
    {                                                                                                                                      \
        if (const auto AnimNotify = AnimUtils::FindNotifyByClass<AnimNotifyNameClass>(Animation))                                          \
        {                                                                                                                                  \
            AnimNotify->OnNotifed.AddUObject(this, CallbackFunction);                                                                      \
        }                                                                                                                                  \
        else                                                                                                                               \
        {                                                                                                                                  \
            const FString AnimNotifyClassName = AnimNotifyNameClass::StaticClass()->GetFName().ToString();                                 \
            const FString AnimationName = Animation->GetFName().ToString();                                                                \
            const FString WarningMessage =                                                                                                 \
                FString::Printf(TEXT("%s anim notify is forgotten to set in animation %s"), *AnimNotifyClassName, *AnimationName);         \
            UE_LOG(LogMeleeAbility_FireBase, Warning, TEXT("%s"), *WarningMessage);                                                        \
        }                                                                                                                                  \
    }

UGameplayAbility_MeleeFireBase::UGameplayAbility_MeleeFireBase()
{
    /* We have some states and delegates so we must instanced */
    InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

    /* In many times attack montage is root and animate to full body */
    ActivationGroup = EAbilityActivationGroup::Exclusive_Blocking;
}

void UGameplayAbility_MeleeFireBase::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

    /* Play character attack montage */
    {
        UAbilityTask_PlayMontageAndWait* AT_Animation = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(
            this, NAME_None, GetCharacterAttackMontage(), 1.0f, NAME_None, false);
        AT_Animation->OnCompleted.AddDynamic(this, &ThisClass::OnAttackRootMotionFinished);

        AT_Animation->OnCancelled.AddDynamic(this, &ThisClass::ForcedEndAbility);
        AT_Animation->OnInterrupted.AddDynamic(this, &ThisClass::ForcedEndAbility);
        AT_Animation->ReadyForActivation();
    }
}

void UGameplayAbility_MeleeFireBase::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
    Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UGameplayAbility_MeleeFireBase::OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
    Super::OnGiveAbility(ActorInfo, Spec);

    BindTDAnimNotify_Dynamic(UTDStartMeleeAttackAnimNotify, CharacterAttackMontage, &ThisClass::OnNotifyStartAttack);
    BindTDAnimNotify_Dynamic(UTDEndMeleeAttackAnimNotify, CharacterAttackMontage, &ThisClass::OnNotifyEndAttack);

    BindTrailAnimNotify_UObject(UTDStartSwordTrailAnimNotify, CharacterAttackMontage, &ThisClass::OnStartTrailVFX);
    BindTrailAnimNotify_UObject(UTDStopSwordTrailAnimNotify, CharacterAttackMontage, &ThisClass::OnStopTrailVFX);
}

void UGameplayAbility_MeleeFireBase::OnRemoveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
    Super::OnRemoveAbility(ActorInfo, Spec);
}

void UGameplayAbility_MeleeFireBase::OnNotifyStartAttack(
    USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
}

void UGameplayAbility_MeleeFireBase::OnNotifyEndAttack(
    USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    if (auto MeleeWeapon = GetMeleeWeapon()) MeleeWeapon->OnEndAttack();
    PrevDamagedTarget = nullptr;
}

void UGameplayAbility_MeleeFireBase::LoopMeleeSphereTrace()
{
    auto* MeleeWeapon = GetMeleeWeapon();
    if (!MeleeWeapon) return;

    auto* Instigator = MeleeWeapon->GetInstigator();

    const auto TraceStart = GetStartPointOfBlade();
    const auto TraceEnd = GetEndPointOfBlade();

    constexpr float SphereTraceRadius = 12.0f;
    const TArray<AActor*> ActorsToIgnore{Instigator, MeleeWeapon};
    FHitResult Hit;

    UKismetSystemLibrary::SphereTraceSingle(GetWorld(), TraceStart, TraceEnd, SphereTraceRadius,                          //
        UEngineTypes::ConvertToTraceType(TraceChannel_Weapon), false, ActorsToIgnore, EDrawDebugTrace::None, Hit, true);  //

    AActor* Target = Hit.GetActor();
    TryApplyAttack(Hit, Target, Instigator);

    DrawWeaponDebug(Hit, TraceStart, TraceEnd);
}

AMeleeWeapon* UGameplayAbility_MeleeFireBase::GetMeleeWeapon()
{
    return Cast<AMeleeWeapon>(GetCurrentWeapon());
}

void UGameplayAbility_MeleeFireBase::TryApplyAttack(const FHitResult& Hit, AActor* const Target, AActor* const Instigator)
{
    if (!Instigator || !Target) return;

    if (Target != PrevDamagedTarget)  // hit only ones
    {
        const auto Weapon = GetCurrentWeapon();
        if (!Weapon) return;

        PrevDamagedTarget = Target;
        const FVector InstigatorLocation = Instigator->GetActorLocation();
        const FVector TargetPosition = Target->GetActorLocation();

        const FVector AttackDirection = (TargetPosition - InstigatorLocation).GetSafeNormal();

        MakePointDamage(Hit, AttackDirection, Weapon->GetWeaponFireData().DamageAmount, Instigator);
    }
}

void UGameplayAbility_MeleeFireBase::ForcedEndAbility()
{
    EndAbilityWithCurrentInfo(true, true);
}

void UGameplayAbility_MeleeFireBase::OnStartTrailVFX(
    USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    if (auto* MeleeWeapon = GetMeleeWeapon()) MeleeWeapon->StartTrailVFX();
}

void UGameplayAbility_MeleeFireBase::OnStopTrailVFX(
    USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
    if (auto* MeleeWeapon = GetMeleeWeapon()) MeleeWeapon->StopTrailVFX();
}

void UGameplayAbility_MeleeFireBase::OnAttackRootMotionFinished()
{
    EndAbilityWithCurrentInfo(true, false);
}

FVector UGameplayAbility_MeleeFireBase::GetStartPointOfBlade()
{
    const auto* MeleeWeapon = GetMeleeWeapon();
    if (!MeleeWeapon) return FVector::ZeroVector;
    return MeleeWeapon->GetStartPointOfBlade();
}

FVector UGameplayAbility_MeleeFireBase::GetEndPointOfBlade()
{
    const auto* MeleeWeapon = GetMeleeWeapon();
    if (!MeleeWeapon) return FVector::ZeroVector;
    return MeleeWeapon->GetEndPointOfBlade();
}