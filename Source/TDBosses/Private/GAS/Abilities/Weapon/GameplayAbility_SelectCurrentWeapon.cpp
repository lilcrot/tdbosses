// Top Down Bosses. Project for learning

#include "GAS/Abilities/Weapon/GameplayAbility_SelectCurrentWeapon.h"
#include "Components/WeaponComponent.h"
#include "Weapons/TDHelper_Weapons.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponComponentAbility, All, All);

UGameplayAbility_SelectCurrentWeapon::UGameplayAbility_SelectCurrentWeapon()
{
    ActivationGroup = EAbilityActivationGroup::Exclusive_Blocking;
}

void UGameplayAbility_SelectCurrentWeapon::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
    const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
    const FGameplayEventData* TriggerEventData)
{
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

    UWeaponComponent* WeaponComponent = UTDHelper_Weapons::GetWeaponComponentFromActor_Check(ActorInfo->OwnerActor.Get());

    SelectWeapon(*WeaponComponent, WeaponComponent->GetCurrentWeaponIndex());
}

void UGameplayAbility_SelectCurrentWeapon::OnEquipFinished()
{
    EndAbilityWithCurrentInfo(true, false);
}

void UGameplayAbility_SelectCurrentWeapon::ForcedEndAbility()
{
    EndAbilityWithCurrentInfo(false, true);
}

void UGameplayAbility_SelectCurrentWeapon::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
    Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UGameplayAbility_SelectCurrentWeapon::SelectNextWeapon(UWeaponComponent& WeaponComponent)
{
    const int Max = WeaponComponent.WeaponClasses.Num() - 1;
    const int Min = 0;
    ++WeaponComponent.CurrentWeaponIndex;

    // scrolling
    WeaponComponent.CurrentWeaponIndex = (WeaponComponent.CurrentWeaponIndex < Min)    ? Min
                                         : (WeaponComponent.CurrentWeaponIndex <= Max) ? WeaponComponent.CurrentWeaponIndex
                                                                                       : Min;

    SelectWeapon(WeaponComponent, WeaponComponent.CurrentWeaponIndex);
}

void UGameplayAbility_SelectCurrentWeapon::SelectWeapon(UWeaponComponent& WeaponComponent, const int32 WeaponIndex)
{
    if (WeaponIndex >= WeaponComponent.WeaponClasses.Num() || WeaponIndex < 0)
    {
        ForcedCurEndAbilityWithWarningLog(true, "Invalid weapon index");
        return;
    }
    if (!WillBeTakenNewWeapon(WeaponComponent, WeaponIndex))
    {
        ForcedCurEndAbilityWithWarningLog(true, "Func SelectWeapon applied to the same weapon");
        return;
    }

    /* Play eqiup montage and wait */
    {
        // get equip anim from weapon that will be selected
        auto* EquipAnimation = WeaponComponent.Weapons[WeaponIndex]->GetWeaponAnimationData().EquipMontage;

        UAbilityTask_PlayMontageAndWait* AT_Animation =
            UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, NAME_None, EquipAnimation, 1.0f, NAME_None, false);
        AT_Animation->OnCancelled.AddDynamic(this, &ThisClass::ForcedEndAbility);
        AT_Animation->OnInterrupted.AddDynamic(this, &ThisClass::ForcedEndAbility);

        AT_Animation->OnCompleted.AddDynamic(this, &ThisClass::OnEquipFinished);
        AT_Animation->ReadyForActivation();
    }
    // Then should executes OnWeaponTaken in weapon component. There will be configured current weapon and etc game logic
}

bool UGameplayAbility_SelectCurrentWeapon::WillBeTakenNewWeapon(const UWeaponComponent& WeaponComponent, const int32 NewWeaponIndex) const
{
    return !WeaponComponent.CurrentWeapon || WeaponComponent.CurrentWeapon != WeaponComponent.Weapons[NewWeaponIndex];
}