#include "GAS/Abilities/Weapon/GameplayAbility_GetNextWeapon.h"
#include "Components/WeaponComponent.h"
#include "Weapons/TDHelper_Weapons.h"

void UGameplayAbility_GetNextWeapon::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    UTDGameplayAbility::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

    UWeaponComponent* WeaponComponent = UTDHelper_Weapons::GetWeaponComponentFromActor_Check(ActorInfo->OwnerActor.Get());
    SelectNextWeapon(*WeaponComponent);
}