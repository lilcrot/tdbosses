// Top Down Bosses. Project for learning

#include "GAS/Abilities/Weapon/GameplayAbility_FireBase.h"
#include "DrawDebugHelpers.h"
#include "Engine/DamageEvents.h"
#include "Weapons/TDHelper_Weapons.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"

namespace TDBossesConsoleVariables
{
static float DrawBulletTraceDuration = 1.0f;
static FAutoConsoleVariableRef CVarDrawBulletTraceDuration(TEXT("TDBosses.Weapon.DrawBulletTraceDuration"), DrawBulletTraceDuration,
    TEXT("Should we do debug drawing for bullet traces (if above zero, sets how long (in seconds))"), ECVF_Default);

static float ThicknessOfTrace = 2.0f;
static FAutoConsoleVariableRef CVarThicknessOfTrace(TEXT("TDBosses.Weapon.ThicknessOfTrace"), ThicknessOfTrace,
    TEXT("When bullet line debug drawing is enabled (see DrawBulletTraceDuration), how big should the thickness be? (in cm)"),
    ECVF_Default);

static float DrawBulletHitDuration = 1.0f;
static FAutoConsoleVariableRef CVarDrawBulletHitDuration(TEXT("TDBosses.Weapon.DrawBulletHitDuration"), DrawBulletHitDuration,
    TEXT("Should we do debug drawing for bullet impacts (if above zero, sets how long (in seconds))"), ECVF_Default);

static float SphereHitRadius = 1.5f;
static FAutoConsoleVariableRef CVarSphereHitRadius(TEXT("TDBosses.Weapon.SphereHitRadius"), SphereHitRadius,
    TEXT("When bullet hit debug drawing is enabled (see DrawBulletHitDuration), how big should the hit radius be?"), ECVF_Default);

static bool bDrawingMissingLines = true;
static FAutoConsoleVariableRef CVarDrawingMissingLines(TEXT("TDBosses.Weapon.DrawingMissingLines"), bDrawingMissingLines,
    TEXT("When bullet doesn't hit anyone (!Hit.bBlockingHit) then draw this line?"), ECVF_Default);

static bool bDrawDebugWeaponLines = false;
static FAutoConsoleVariableRef CVarDrawDebugWeaponLines(
    TEXT("TDBosses.Weapon.DrawDebugWeaponLines"), bDrawDebugWeaponLines, TEXT("Draw debug weapon line trace?"), ECVF_Default);

}  // namespace TDBossesConsoleVariables

void UGameplayAbility_FireBase::DrawWeaponDebug(const FHitResult& Hit, const FVector& TraceStart, const FVector& TraceEnd)
{
    using namespace TDBossesConsoleVariables;
    if (!bDrawDebugWeaponLines) return;

    if (Hit.bBlockingHit)  // this more beauty: line trace doesn't go out of impact point
    {
        if (DrawBulletTraceDuration > 0.0f)
        {
            DrawDebugLine(GetWorld(), TraceStart, Hit.ImpactPoint, FColor::Red,  //
                false, DrawBulletTraceDuration, 0, ThicknessOfTrace);            //
        }
        if (DrawBulletHitDuration > 0.0f)
        {
            DrawDebugSphere(GetWorld(), Hit.ImpactPoint, SphereHitRadius, 0,      //
                FColor::Yellow, false, DrawBulletHitDuration, ThicknessOfTrace);  //
        }
    }
    else if (bDrawingMissingLines && DrawBulletTraceDuration > 0.0f)
    {
        DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, DrawBulletTraceDuration, 0, ThicknessOfTrace);
    }
}

UGameplayAbility_FireBase::UGameplayAbility_FireBase()
{
    ActivationGroup = EAbilityActivationGroup::Exclusive_Replaceable;
}

void UGameplayAbility_FireBase::PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate,
    const FGameplayEventData* TriggerEventData)
{
    // Based on the architecture of project: actor that own weapons must have this ability but not a weapon.
    CurrentWeapon = UTDHelper_Weapons::GetCurrentWeaponFromActor(ActorInfo->OwnerActor.Get());
    if (!CurrentWeapon)
    {
        ForcedEndAbilityWithWarningLog(Handle, ActorInfo, ActivationInfo, false,           //
            "CurrentWeapon is null, check ActorInfo that you transfer to this ability.");  //
        return;
    }
    Super::PreActivate(Handle, ActorInfo, ActivationInfo, OnGameplayAbilityEndedDelegate, TriggerEventData);
}

void UGameplayAbility_FireBase::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
    Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
}

void UGameplayAbility_FireBase::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
    const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
    CurrentWeapon = nullptr;
    Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

bool UGameplayAbility_FireBase::MakePointDamage(
    const FHitResult& Hit, const FVector& ShotDirection, const float DamageAmount, AActor* Instigator)
{
    FPointDamageEvent PointDamageEvent;

    PointDamageEvent.HitInfo = Hit;
    PointDamageEvent.ShotDirection = ShotDirection;

    const auto MyPawn = Cast<APawn>(Instigator);
    if (!MyPawn) return false;

    Hit.GetActor()->TakeDamage(DamageAmount, PointDamageEvent, MyPawn->GetController(), Instigator);
    return true;
}

bool UGameplayAbility_FireBase::ApplyRadialDamage(const FVector& Origin, const float DamageRadius, bool bDoFullDamage, AActor* Instigator,
    const float Damage, ECollisionChannel DamageChannel)
{
    const auto MyPawn = Cast<APawn>(Instigator);
    if (!MyPawn) return false;
    const auto Controller = MyPawn->GetController();

    const TArray<AActor*> IgnoreActors{Instigator, GetCurrentWeapon()};

    UGameplayStatics::ApplyRadialDamage(GetWorld(), Damage, Origin, DamageRadius, nullptr,  //
        IgnoreActors, Instigator, Controller, bDoFullDamage, DamageChannel);                //

    return true;
}

ATDBaseWeapon* UGameplayAbility_FireBase::GetCurrentWeapon() const
{
    return CurrentWeapon;
}

void UGameplayAbility_FireBase::DecreaseAmmo(ATDBaseWeapon& Weapon)
{
    Weapon.AmmoData.DecreaseCurrentBullets();
}

void UGameplayAbility_FireBase::SetCurrentBullets(ATDBaseWeapon& Weapon, const int32 NewValue)
{
    Weapon.AmmoData.SetCurrentBullets(NewValue);
}
