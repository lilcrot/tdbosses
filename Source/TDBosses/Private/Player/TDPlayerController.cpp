// Top Down Bosses. Project for learning

#include "Player/TDPlayerController.h"
#include "Player/PlayerCharacter.h"
#include "Components/HealthComponent.h"
#include "Components/WeaponComponent.h"
#include "Weapons/TDBaseWeapon.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDCoreTypes.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Components/InputComponent.h"
#include "EnhancedInputComponent.h"
#include "Weapons/TDHelper_Weapons.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDPlayerController, All, All);

ATDPlayerController::ATDPlayerController() {}

void ATDPlayerController::BeginPlay()
{
    Super::BeginPlay();
}

void ATDPlayerController::OnPossess(APawn* aPawn)
{
    Super::OnPossess(aPawn);
    UWeaponComponent* PawnWeaponComponent = UTDHelper_Weapons::GetWeaponComponentFromActor(aPawn);
    if (PawnWeaponComponent)
    {
        PawnWeaponComponent->OnCurrentWeapon_StartFire.AddUObject(this, &ThisClass::OnPlayerStartFire);
        PawnWeaponComponent->OnCurrentWeapon_StopFire.AddUObject(this, &ThisClass::OnPlayerStopFire);
    }
}

void ATDPlayerController::OnUnPossess()
{
    UWeaponComponent* PawnWeaponComponent = UTDHelper_Weapons::GetWeaponComponentFromActor(GetPawn());
    if (PawnWeaponComponent)
    {
        PawnWeaponComponent->OnCurrentWeapon_StartFire.RemoveAll(this);
        PawnWeaponComponent->OnCurrentWeapon_StopFire.RemoveAll(this);
    }
    Super::OnUnPossess();
}

AActor* ATDPlayerController::GetBestTarget(TArray<AActor*> Actors, FVector2D Direction)
{
    float BestCrossProduct = -FLT_MAX;
    AActor* BestActor = nullptr;
    const AActor* MyPlayer = Cast<AActor>(GetPawn());

    for (const auto Actor : Actors)
    {
        if (Actor == MyPlayer) continue;

        const auto HealthComponent = Actor->FindComponentByClass<UHealthComponent>();
        if (HealthComponent && !HealthComponent->IsDead())
        {
            FVector2D ActorPosition = FVector2D(Actor->GetActorLocation());
            float CrossProduct = FVector2D::CrossProduct(Direction, ActorPosition);

            if (CrossProduct > BestCrossProduct)
            {
                BestActor = Actor;
                BestCrossProduct = CrossProduct;
            }
        }
    }
    return BestActor;
}

bool ATDPlayerController::RotateCharacterToEnemy(AActor* Target)
{
    UWorld* World = GetWorld();
    if (!World) return false;

    const auto MyPlayer = Cast<APlayerCharacter>(GetPawn());
    if (!MyPlayer || !Target) return false;
    const auto CurrentPlayerWeapon = MyPlayer->GetCurrentWeapon();
    if (!CurrentPlayerWeapon) return false;

    FHitResult Hit;
    FCollisionQueryParams CollisionQueryParams;
    CollisionQueryParams.AddIgnoredActor(MyPlayer);
    CollisionQueryParams.AddIgnoredActor(CurrentPlayerWeapon);

    World->LineTraceSingleByChannel(Hit, MyPlayer->GetWeaponSocketLocation(), Target->GetActorLocation(),  //
        ECC_Visibility, CollisionQueryParams);                                                             //
    const bool bHasObstacles = !(Hit.GetActor() == Target);

    if (!bHasObstacles)
    {
        // We need calculate weapon offset regarding the player
        const FVector WeaponSocketLocaiton = MyPlayer->GetWeaponSocketLocation();
        const FVector OffsetAmount = WeaponSocketLocaiton - MyPlayer->GetActorLocation();

        const FWeaponEquipAttachSocket WeaponSocket = MyPlayer->GetWeaponSocketStruct();
        // take into account the location of the weapon
        const FVector DesiredOffsetDirection = WeaponSocket.bIsRightHand ? FVector::RightVector : -FVector::RightVector;
        const FVector WeaponOffset = DesiredOffsetDirection * OffsetAmount;
        const FVector AdjustedWeaponLocation =
            WeaponSocketLocaiton + MyPlayer->GetActorTransform().TransformVector(WeaponOffset);  // take into account the player's transform

        const FVector TargetDirection = Target->GetActorLocation() - AdjustedWeaponLocation;

        // Using Player Forward allows us to align the muzzle of the player's weapon to the target,
        // taking into account the current direction of the player. If take ActorLocation player just look at target.
        const FVector PlayerForward = MyPlayer->GetActorForwardVector();

        // Rotate only on Z (yaw) axis
        const FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(PlayerForward, TargetDirection);

        MyPlayer->RotateToEnemyWithLerp(NewRotation);
        return true;
    }

    return false;
}

void ATDPlayerController::ChangeTargetByInputDirection(const FInputActionValue& Value)
{
    const auto MyPlayer = Cast<APlayerCharacter>(GetPawn());
    if (!MyPlayer) return;

    const FVector2D InputVector = Value.Get<FVector2D>();
    const auto Target = GetBestTarget(MyPlayer->GetTargettingOverlapped(), InputVector);

    if (RotateCharacterToEnemy(Target))
    {
    }  // if target is valid & player can rotate

    else  // no one enemy or has obstacles, we add rotation by input direction (rotate around your self axes)
    {
        // forward & left = rotate to left | backward & right = rotate to right
        const FVector2D Forward = FVector2D(0.0f, 1.0f);
        const FVector2D Left = FVector2D(-1.0f, 0.0f);
        float RotationAngle = 20.0f;

        if (InputVector.Equals(Forward) || InputVector.Equals(Left))
        {
            RotationAngle = -20.0f;
        }
        const FRotator NewRotation = FRotator(0.0f, MyPlayer->GetActorRotation().Yaw + RotationAngle, 0.0f);
        MyPlayer->RotateCharacterWithLerp(NewRotation);
    }
}

void ATDPlayerController::SetupPlayerInputToShootingContext(const APlayerCharacter& MyPlayer)
{
    auto InputSubsystem = MyPlayer.InputSubsystem;
    if (!InputSubsystem) return;

    InputSubsystem->ClearAllMappings();
    InputSubsystem->AddMappingContext(MyPlayer.ShootingContext, 0);

    auto EnhancedInputComponent = GetEnhancedInputComponent(MyPlayer);
    if (!EnhancedInputComponent) return;

    EnhancedInputComponent->BindAction(ChangeTargetAction, ETriggerEvent::Triggered,  //
        this, &ThisClass::ChangeTargetByInputDirection);                              //
}

void ATDPlayerController::ResetPlayerInputToDefault(const APlayerCharacter& MyPlayer)
{
    auto InputSubsystem = MyPlayer.InputSubsystem;
    if (!InputSubsystem) return;

    InputSubsystem->ClearAllMappings();
    InputSubsystem->AddMappingContext(MyPlayer.TDBCharacterContext, 1);  // main by default
    InputSubsystem->AddMappingContext(MyPlayer.ShootingContext, 0);
}

UEnhancedInputComponent* ATDPlayerController::GetEnhancedInputComponent(const APlayerCharacter& MyPlayer)
{
    const auto PlayerInputComponent = MyPlayer.InputComponent;
    if (!PlayerInputComponent) return nullptr;

    return CastChecked<UEnhancedInputComponent>(PlayerInputComponent);
}

void ATDPlayerController::OnPlayerStartFire()
{
    const auto MyPlayer = Cast<APlayerCharacter>(GetPawn());
    if (!MyPlayer) return;

    const ATDBaseWeapon* PawnWeapon = UTDHelper_Weapons::GetCurrentWeaponFromActor(MyPlayer);
    if (!PawnWeapon) return;

    const auto& WeaponFireData = PawnWeapon->GetWeaponFireData();

    /* enable auto-target system only for ranged weapon */
    if (WeaponFireData.Type == EWeaponTypes::Ranged)
    {
        SetIgnoreMoveInput(true);

        SetupPlayerInputToShootingContext(*MyPlayer);

        RotateCharacterToEnemy(GetBestTarget(MyPlayer->GetTargettingOverlapped()));
    }
}

void ATDPlayerController::OnPlayerStopFire()
{
    const auto MyPlayer = Cast<APlayerCharacter>(GetPawn());
    if (!MyPlayer) return;

    const ATDBaseWeapon* PawnWeapon = UTDHelper_Weapons::GetCurrentWeaponFromActor(MyPlayer);
    if (!PawnWeapon) return;

    const auto& WeaponFireData = PawnWeapon->GetWeaponFireData();

    /* reset limits of inputs for only ranged weapon because only their inputs changes */
    if (WeaponFireData.Type == EWeaponTypes::Ranged)
    {
        ResetPlayerInputToDefault(*MyPlayer);
        ResetIgnoreMoveInput();
    }
}