// Top Down Bosses. Project for learning

#include "Player/PlayerCharacter.h"
#include "Player/TDPlayerController.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Components/WeaponComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GAS/TDAbilitySystemComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogPlayerCharacter, All, All);

// Sets default values
APlayerCharacter::APlayerCharacter()
{
    SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent");
    SpringArmComponent->SetupAttachment(GetRootComponent());
    SpringArmComponent->SocketOffset = FVector(-150.0f, 400.0f, 700.0f);
    SpringArmComponent->bUsePawnControlRotation = true;  // Rotate the arm based on the controller

    CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
    CameraComponent->SetupAttachment(SpringArmComponent);
    CameraComponent->SetRelativeRotation(FRotator(-50.0f, -40.0f, 0.0f));
    CameraComponent->bUsePawnControlRotation = false;  // Camera does not rotate relative to arm

    bUseControllerRotationYaw = false;  // Top Down Camera mustn't move, character must move and camera follow him
}

void APlayerCharacter::BeginPlay()
{
    Super::BeginPlay();

    /* Initial settup player input controllers */
    if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
    {
        InputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
        if (InputSubsystem)
        {
            InputSubsystem->AddMappingContext(TDBCharacterContext, 1);
            InputSubsystem->AddMappingContext(ShootingContext, 0);

            IntilizeInputAbilities();  // because without contexts this function is useless
        }
    }
}

void APlayerCharacter::Move(const FInputActionValue& Value)
{
    FVector2D MovementVector = Value.Get<FVector2D>();

    if (Controller != nullptr)
    {
        // find out which way is forward
        const FRotator Rotation = Controller->GetControlRotation();
        const FRotator YawRotation(0, Rotation.Yaw, 0);

        const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
        const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

        AddMovementInput(ForwardDirection, MovementVector.Y);
        AddMovementInput(RightDirection, MovementVector.X);
    }
}

void APlayerCharacter::OnDeath()
{
    Super::OnDeath();
    GetCharacterMovement()->DisableMovement();
    DisableInput(Cast<APlayerController>(Controller));
}

void APlayerCharacter::AddStartupGameplayAbilities()
{
    Super::AddStartupGameplayAbilities();
    bAbilityInitialized = true;
}

void APlayerCharacter::IntilizeInputAbilities()
{
    UTDAbilitySystemComponent* TD_ASC = GetTDAbilitySystemComponent();
    if (!TD_ASC) return;

    if (!TD_ASC->HasBegunPlay())
    {
        UE_LOG(LogPlayerCharacter, Warning,
            TEXT("TDAbilitySystemComponent hasn't begun play => InputComponent in this component will be null"));
        return;
    }

    for (const TPair<UInputAction*, TSubclassOf<UGameplayAbility>>& AbilityPair : InputAbilities)
    {
        constexpr int32 InLevel = 0;
        constexpr int32 InInputID = 0;  // InputID will be settuped by ASC

        const auto& AbilityHandle = TD_ASC->GiveAbility(FGameplayAbilitySpec(AbilityPair.Value, InLevel, InInputID, this));
        TD_ASC->SetInputBinding(AbilityPair.Key, AbilityHandle);
    }
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent);
    if (!EnhancedInputComponent) return;

    if (TDBCharacterContext)  // priority: 1
    {
        EnhancedInputComponent->BindAction(MovementAction, ETriggerEvent::Triggered, this, &ThisClass::Move);

        EnhancedInputComponent->BindAction(NextWeaponAction, ETriggerEvent::Triggered, WeaponComponent, &UWeaponComponent::NextWeapon);
        EnhancedInputComponent->BindAction(ReloadAction, ETriggerEvent::Started, WeaponComponent, &UWeaponComponent::TryReload);
    }
    if (ShootingContext)  // priority: 0
    {
        EnhancedInputComponent->BindAction(DefaultFireAction, ETriggerEvent::Started, this, &ATDCharacter::DefaultFire);
        EnhancedInputComponent->BindAction(DefaultFireAction, ETriggerEvent::Completed, this, &ATDCharacter::StopFire);

        EnhancedInputComponent->BindAction(SpecialFireAction, ETriggerEvent::Started, this, &ATDCharacter::SpecialFire);
        EnhancedInputComponent->BindAction(SpecialFireAction, ETriggerEvent::Completed, this, &ATDCharacter::StopFire);
    }
}
