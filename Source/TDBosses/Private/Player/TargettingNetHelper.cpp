// Top Down Bosses. Project for learning

#include "Player/TargettingNetHelper.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATargettingNetHelper::ATargettingNetHelper()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;

    TargetVolume = CreateDefaultSubobject<USphereComponent>("TargetVolume");
    SetRootComponent(TargetVolume);
    TargetVolume->InitSphereRadius(1000.0f);

    /*Generate events only for Pawns*/
    TargetVolume->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    TargetVolume->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
    TargetVolume->SetGenerateOverlapEvents(true);
}

TArray<AActor*> ATargettingNetHelper::GetOverlappedTargets() const
{
    TArray<AActor*> MyOverlappingActors;
    GetOverlappingActors(MyOverlappingActors, APawn::StaticClass());

    // remove owner from targets
    MyOverlappingActors.Remove(GetOwner());

    return MyOverlappingActors;
}

void ATargettingNetHelper::BeginPlay()
{
    Super::BeginPlay();

    auto* MyOwner = GetOwner();

    const FString OwnerErrorMessage = "SetOwner is must be set when TargettingNetHellper is spawned. Use helper function: "
                                      "SpawnTargettingNetHelper that declared in header of this class";
    checkf(MyOwner, TEXT("%s"), *OwnerErrorMessage);

    FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
    AttachToActor(MyOwner, AttachmentRules);

    MyOwner->OnEndPlay.AddDynamic(this, &ThisClass::OnOwnerEndPlay);
}

void ATargettingNetHelper::OnOwnerEndPlay(AActor* Actor, const EEndPlayReason::Type EndPlayReason)
{
    Destroy();  // destroy when owner who own this net hellper is destroyed
}
