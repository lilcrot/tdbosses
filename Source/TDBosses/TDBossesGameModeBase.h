// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDBossesGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class TDBOSSES_API ATDBossesGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    ATDBossesGameModeBase();
};
