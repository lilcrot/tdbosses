// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDBossesGameModeBase.h"
#include "Player/PlayerCharacter.h"
#include "UI/TDGameHUD.h"
#include "Player/TDPlayerController.h"

ATDBossesGameModeBase::ATDBossesGameModeBase()
{
    DefaultPawnClass = APlayerCharacter::StaticClass();
    HUDClass = ATDGameHUD::StaticClass();
    PlayerControllerClass = ATDPlayerController::StaticClass();
}
