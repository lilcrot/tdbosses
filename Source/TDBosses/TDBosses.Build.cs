// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDBosses : ModuleRules
{
    public TDBosses(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[]
        { 
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "EnhancedInput",
            "Niagara",
            "PhysicsCore",
            "GameplayAbilities", "GameplayTags", "GameplayTasks",
            "Json", "JsonUtilities"
        });

        PublicIncludePaths.AddRange(new string[]
        {
            "TDBosses/Public/Components",
            "TDBosses/Public/Player",
            "TDBosses/Public/Weapons",
            "TDBosses/Public/Animations",
            "TDBosses/Public/UI", 
            "TDBosses/Public/GAS",
            "TDBosses/Public/Tests"
        });

        PrivateDependencyModuleNames.AddRange(new string[] {});
    }

}
