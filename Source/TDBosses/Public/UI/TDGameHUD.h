// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TDGameHUD.generated.h"

/**
 *
 */
UCLASS()
class TDBOSSES_API ATDGameHUD : public AHUD
{
    GENERATED_BODY()

protected:
    virtual void BeginPlay() override;
};
