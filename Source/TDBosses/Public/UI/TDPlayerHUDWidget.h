// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDPlayerHUDWidget.generated.h"

/**
 *
 */
UCLASS()
class TDBOSSES_API UTDPlayerHUDWidget : public UUserWidget
{
    GENERATED_BODY()
};
