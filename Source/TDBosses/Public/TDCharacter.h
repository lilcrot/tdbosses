#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDCoreTypes.h"
#include "Components/WeaponComponent.h"
#include "Components/TimelineComponent.h"
#include "AbilitySystemInterface.h"

#include "TDCharacter.generated.h"

class UHealthComponent;
class ATDBaseWeapon;
class UCurveFloat;
class ATargettingNetHelper;
class UTDAbilitySystemComponent;
class UGameplayAbility;

// transfer new rotation
DECLARE_MULTICAST_DELEGATE_OneParam(FOnCharacterRotationToEnemyEnd, FRotator);

USTRUCT(BlueprintType)
struct FDeathVaribles
{
    GENERATED_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health|Death", meta = (Units = "s", ClampMin = "0.1"))
    float LifeSpanOnDeath = 3.5f;

    UPROPERTY(EditDefaultsOnly, Category = "Health|Death|Animations")
    TArray<UAnimMontage*> DeathMontages;

    UPROPERTY(EditDefaultsOnly, Category = "Health|Death")
    FName RagdollImpulseBone = "Pelvis";

    /*With which strenght  character will impulse when died*/
    UPROPERTY(EditDefaultsOnly, Category = "Health|Death")
    float ImpulseStrenght = 500.0f;
};

UCLASS()
class TDBOSSES_API ATDCharacter : public ACharacter, public IAbilitySystemInterface
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    ATDCharacter();

    virtual void Tick(float DeltaSeconds) override;

    UFUNCTION(BlueprintPure, Category = "Components")
    UWeaponComponent* GetWeaponComponent() const { return WeaponComponent; }

    UFUNCTION(BlueprintPure, Category = "Weapon")
    const FWeaponEquipAttachSocket& GetWeaponSocketStruct() const { return WeaponComponent->EquipAttachSocketName; };

    UFUNCTION(BlueprintPure, Category = "Weapon")
    FVector GetWeaponSocketLocation() const;

    UFUNCTION(BlueprintPure, Category = "Weapon")
    ATDBaseWeapon* GetCurrentWeapon() const { return WeaponComponent->GetCurrentWeapon(); };

    /* Get TargettingNetHelper (that interacts and saves enemies for auto-target system)*/
    UFUNCTION(BlueprintPure, Category = "TargetSystem")
    const ATargettingNetHelper* GetTargettingNetHelper() const;

    /* Get overrlapped actors by TargettingNetHelper */
    UFUNCTION(BlueprintPure, Category = "TargetSystem")
    TArray<AActor*> GetTargettingOverlapped() const;

protected:
    //---------------------
    // Character's functions
    //---------------------

    virtual void BeginPlay() override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

    virtual void PossessedBy(AController* NewController) override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components") UHealthComponent* HealthComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UWeaponComponent* WeaponComponent;

    UPROPERTY()
    ATargettingNetHelper* TargettingNetHelper;

public:
    //-------------------------------
    // Gameplay Ability System (GAS)
    //-------------------------------

    UFUNCTION(BlueprintPure)
    virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
    UFUNCTION(BlueprintPure)
    UTDAbilitySystemComponent* GetTDAbilitySystemComponent() const;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UTDAbilitySystemComponent* TDAbilitySystemComponent;

    virtual void AddStartupGameplayAbilities();  // give(initilize) all abilities for AbilitySystemComponent

    bool bAbilityInitialized;

public:
    //-----------------
    //  Fire abilities
    //-----------------
    UFUNCTION(BlueprintPure, Category = "Fire")
    bool GetIsFiring() const;

    UFUNCTION(BlueprintCallable)
    void DefaultFire();
    UFUNCTION(BlueprintCallable)
    void SpecialFire();

    UFUNCTION(BlueprintCallable)
    void StopFire();

protected:
    //---------
    //  Death
    //---------
    UFUNCTION()
    virtual void OnDeath();
    void Ragdoll();

    UFUNCTION(BlueprintNativeEvent)
    void DeathVisual();

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health|Death")
    FDeathVaribles DeathVaribles;

public:
    //-----------------
    //  RotateWithLerp
    //-----------------
    UFUNCTION(BlueprintCallable)
    void SetLerpRotationInProgress(bool NewValue) { bLerpRotationInProgress = NewValue; }

    /* Rotate character to new rotaion
     * Note: Make sure that if player can cancel it rotaton(e.g smth input) you set flag variable (see: SetLerpRotationInProgress) to
     * false*/
    UFUNCTION(BlueprintCallable)
    void RotateCharacterWithLerp(FRotator NewRotationIn);

    /* The same as RotateCharacterWithLerp but with necessary deletagets execute */
    UFUNCTION(BlueprintCallable)
    void RotateToEnemyWithLerp(const FRotator NewRotationIn);

    FOnCharacterRotationToEnemyEnd OnCharacterRotationToEnemyEnd;

protected:
    FTimeline RotateToEnemyTimeLine;
    FTimeline RotateCharacterWithLerpTimeLine;

    UPROPERTY(EditDefaultsOnly, Category = "TimeLines")
    UCurveFloat* RotateWithLerpCurveFloat;

private:
    UFUNCTION()
    void OnCharacterRotationToEnemyFinished();
    UFUNCTION()
    void UpdateRotateWithLerp(float AlphaAmount);
    UFUNCTION()
    void ClearRotationWithLerp();

    UPROPERTY()                    // for garbage collector
    FRotator NewRotationTimeLine;  // temp variable to RotateToEnemyTimeLine

    bool bLerpRotationInProgress;  // flag variable

    bool BindTimelines();
};
