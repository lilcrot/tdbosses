// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDCoreTypes.h"
#include "AbilitySystemInterface.h"
#include "GAS/Abilities/Weapon/GameplayAbility_SelectCurrentWeapon.h"

#include "WeaponComponent.generated.h"

/*------------WeaponComponent------------
 *
 *In this WeaponComponent not implied discard weapons.
 *
 *Weapons are set from WeaponClasses by default
 *
 *Weapons that are unequipped are invisible, they don't delete because a large number of weapons on character are not implied.
 *
 *------------WeaponComponent------------*/

DECLARE_MULTICAST_DELEGATE(FOnCurrentWeaponStartFire);
DECLARE_MULTICAST_DELEGATE(FOnCurrentWeaponStopFire);

class ATDBaseWeapon;
class UAbilitySystemComponent;
class UGameplayAbility_GetNextWeapon;

// @param NameFunc: it's must match with weapon declaration in header
// (that is this func must be in base weapon header)
// WARNING: be safety after that macro all members will be in public, make sure it doesn't breaks encapsulation!
#define Declare_WeaponFireAction(NameFunc)                                                                                                 \
public:                                                                                                                                    \
    void NameFunc();                                                                                                                       \
                                                                                                                                           \
protected:                                                                                                                                 \
    void NameFunc(ATDBaseWeapon& Weapon);                                                                                                  \
                                                                                                                                           \
public:

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent, IsBlueprintBase = "true"))
class TDBOSSES_API UWeaponComponent : public UActorComponent, public IAbilitySystemInterface
{
    GENERATED_BODY()

    friend class UGameplayAbility_SelectCurrentWeapon;

public:
    //-----------------------------
    // Weapon's private functions
    //-----------------------------
    Declare_WeaponFireAction(DefaultFire);
    Declare_WeaponFireAction(SpecialFire);

    void StopFire();

    void TryReload();

    //----------------
    // WeaponComponent
    //----------------

    // Sets default values for this component's properties
    UWeaponComponent();

    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    void NextWeapon();

    bool TryToAddWeapon(UClass* WeaponClass);

    UFUNCTION(BlueprintPure, Category = "Weapon")
    ATDBaseWeapon* GetCurrentWeapon() const;

    UFUNCTION(BlueprintPure, Category = "Weapon")
    int32 GetCurrentWeaponIndex() const { return CurrentWeaponIndex; };

    UFUNCTION(BlueprintPure, Category = "Weapon")
    bool GetIsFiring() const;

    UFUNCTION(BlueprintCallable, Category = "Weapon")
    void HideAllWeapon();

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    FWeaponEquipAttachSocket EquipAttachSocketName{"weapon_rSocket", true};

    UFUNCTION(BlueprintPure)
    virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

    FOnCurrentWeaponStartFire OnCurrentWeapon_StartFire;

    // called by weapon directly because only they know when they ended fire
    // (e.g melee weapon only knows when they end attack montage => attack)
    FOnCurrentWeaponStopFire OnCurrentWeapon_StopFire;

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

    /* Classes that owner has */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    TArray<TSubclassOf<ATDBaseWeapon>> WeaponClasses;

    void SpawnWeapons();

    /* This gameplay ability class will acivate in func: NextWeapon */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    TSubclassOf<UGameplayAbility_GetNextWeapon> GA_NextWeaponClass;

    /* This gameplay ability class will acivate in func: SelectWeaponByCurrentIndex */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
    TSubclassOf<UGameplayAbility_SelectCurrentWeapon> GA_SelectCurrentWeaponClass;

private:
    ATDBaseWeapon* CurrentWeapon;
    TArray<ATDBaseWeapon*> Weapons;
    int32 CurrentWeaponIndex = 0;

    void AttachWeaponToCharacter(ATDBaseWeapon* Weapon, FName Socket);

    ATDBaseWeapon* SpawnWeapon(UClass* WeaponClass);  // spawn and attach to character

    void SelectWeaponByCurrentIndex();

    bool CanFire() const;

    void BindWeaponNotifies(ATDBaseWeapon* Weapon);
    void OnWeaponTaken(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference);

    UPROPERTY(transient)
    UAbilitySystemComponent* OwnerASC;
    void InitOwnerASC();

    void StopFire(ATDBaseWeapon& Weapon);
};
