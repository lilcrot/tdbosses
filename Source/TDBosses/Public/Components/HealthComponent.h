// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

/*------------HealthComponent------------
 *
 *HealthComponent signed to events of Owner(AActor)
 *In functions that signed to events - own delegates are broadcast
 *
 *Functionality of death or smth else should implement in Owner
 *
 *Also component have invulnerability functionality
 *
 *------------HealthComponent------------
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeathSignature);

/* transfers new health, if health will set to <= 0 then will callback OnDeath and never this  */
DECLARE_MULTICAST_DELEGATE_OneParam(FOnHealthChangedSignature, int);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_NineParams(FOnTakenPointHitSignature, AActor*, DamagedActor, float, Damage, class AController*,
    InstigatedBy, FVector, HitLocation, class UPrimitiveComponent*, FHitComponent, FName, BoneName, FVector, ShotFromDirection,
    const class UDamageType*, DamageType, AActor*, DamageCauser);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_SevenParams(FOnTakenRadialHitSignature, AActor*, DamagedActor, float, Damage, const class UDamageType*,
    DamageType, FVector, Origin, FHitResult, HitInfo, class AController*, InstigatedBy, AActor*, DamageCauser);

class UCameraShakeBase;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDBOSSES_API UHealthComponent : public UActorComponent
{
    GENERATED_BODY()
public:
    // Sets default values for this component's properties
    UHealthComponent();

    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    /* safe set health with clamp between zero and max health */
    UFUNCTION(BlueprintCallable, Category = "Health")
    void SetHealth(int32 NewHealth);

    UFUNCTION(BlueprintPure, Category = "Health")
    int32 GetHealth() const { return CurrentHealth; }

    /* return health limit */
    UFUNCTION(BlueprintPure, Category = "Health")
    int32 GetMaxHealth() const { return MaxHealth; }

    UFUNCTION(BlueprintPure, Category = "Health")
    bool IsDead() const { return CurrentHealth <= 0; }

    /* By this tag will executes gameplay cue to owner */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health|TakeHit")
    FGameplayTag HitReactGameplayCueTag;

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
    int32 MaxHealth = 4;

    UPROPERTY(EditDefaultsOnly, Category = "Health|TakeHit")
    TSubclassOf<UCameraShakeBase> CameraShake;

private:
    int CurrentHealth;

    void PlayCameraShake();

public:
    //-------------------------
    // Hit & EventsSignatures
    //-------------------------

    UPROPERTY(BlueprintAssignable)
    FOnDeathSignature OnDeath;

    FOnHealthChangedSignature OnHealthChangedDelegate;

    UPROPERTY(BlueprintAssignable)
    FOnTakenPointHitSignature OnTakenPointHit;

    UPROPERTY(BlueprintAssignable)
    FOnTakenRadialHitSignature OnTakenRadialHit;

protected:  // delegates callback functions
    UFUNCTION()
    void OnTakeAnyDamage(
        AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

    UFUNCTION()
    void OnTakePointDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation,  //
        class UPrimitiveComponent* FHitComponent, FName BoneName,                                                     //
        FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser);                        //

    UFUNCTION()
    void OnTakeRadialDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin,  //
        FHitResult HitInfo, class AController* InstigatedBy, AActor* DamageCauser);

    void OnHealthChanged(int32 NewHealth);

public:
    //------------------
    // Invulnerability
    //------------------

    void SetInvulnerability();

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health|TakeHit|Invulnerability")
    bool bCanInvulnerable = false;

    /* Return current state invulnerable or not */
    UFUNCTION(BlueprintPure, Category = "Health")
    bool IsInvulnerable() const { return bInvulnerability; }

protected:
    /* How many seconds player will be in invulnerability after hit */
    UPROPERTY(EditAnywhere, Category = "Health|TakeHit|Invulnerability", meta = (EditCondition = "bCanInvulnerable"))
    float TimeOfInvulnerability = 3.0f;

private:
    bool bInvulnerability;
    void ResetInvulnerability();
    FTimerHandle InvulnerabilityTimer;
};
