#pragma once
#include "CoreMinimal.h"

#pragma region TDNotifiesIncludes

#include "Animations/Notifies/TDAnimNotify.h"
#include "Animations/Notifies/Weapon/TDStartMeleeAttackAnimNotify.h"
#include "Animations/Notifies/Weapon/TDEndMeleeAttackAnimNotify.h"
#include "Animations/Notifies/Weapon/TDStopSwordTrailAnimNotify.h"
#include "Animations/Notifies/Weapon/TDStartSwordTrailAnimNotify.h"
#include "Animations/Notifies/Weapon/TDWeaponTakenAnimNotify.h"
#include "Animations/Notifies/Weapon/TDJumpRadialDamageAnimNotify.h"

#pragma endregion TDNotifiesIncludes

// @param AnimNotifyNameClass: without StaticClass(), only name, e.g TDAnimNotify
// @param CallbackFunction: format are OwnerFuncClassName::Func, e.g &ThisClass::OnAnimNotify
#define BindTDAnimNotify_UObject(AnimNotifyNameClass, Animation, CallbackFunction)                                                         \
    {                                                                                                                                      \
        if (const auto AnimNotify = AnimUtils::FindNotifyByClass<AnimNotifyNameClass>(Animation))                                          \
        {                                                                                                                                  \
            AnimNotify->OnNotifed.AddUObject(this, CallbackFunction);                                                                      \
        }                                                                                                                                  \
        else                                                                                                                               \
        {                                                                                                                                  \
            const FString AnimNotifyClassName = AnimNotifyNameClass::StaticClass()->GetFName().ToString();                                 \
            const FString AnimationName = Animation->GetFName().ToString();                                                                \
            const FString ErrorMessage = FString::Printf(TEXT("%s in %s forgotten to set."), *AnimNotifyClassName, *AnimationName);        \
            checkf(AnimNotify, *ErrorMessage);                                                                                             \
        }                                                                                                                                  \
    }

// @param AnimNotifyNameClass: without StaticClass(), only name, e.g TDAnimNotify
// @param CallbackFunction must be with UFUNCTION: format are OwnerFuncClassName::Func, e.g &ThisClass::OnAnimNotify
#define BindTDAnimNotify_Dynamic(AnimNotifyNameClass, Animation, CallbackFunction)                                                         \
    {                                                                                                                                      \
        if (const auto AnimNotify = AnimUtils::FindNotifyByClass<AnimNotifyNameClass>(Animation))                                          \
        {                                                                                                                                  \
            AnimNotify->BP_OnNotified.AddDynamic(this, CallbackFunction);                                                                  \
        }                                                                                                                                  \
        else                                                                                                                               \
        {                                                                                                                                  \
            const FString AnimNotifyClassName = AnimNotifyNameClass::StaticClass()->GetFName().ToString();                                 \
            const FString AnimationName = Animation->GetFName().ToString();                                                                \
            const FString ErrorMessage =                                                                                                   \
                FString::Printf(TEXT("%s TDAnimNotify or %s animation forgotten to set."), *AnimNotifyClassName, *AnimationName);          \
            checkf(AnimNotify, *ErrorMessage);                                                                                             \
        }                                                                                                                                  \
    }

namespace AnimUtils
{
template <typename T>
T* FindNotifyByClass(UAnimSequenceBase* Animation)
{
    if (!Animation) return nullptr;

    const auto NotifyEvents = Animation->Notifies;
    for (auto NotifyEvent : NotifyEvents)
    {
        auto AnimNotify = Cast<T>(NotifyEvent.Notify);
        if (AnimNotify)
        {
            return AnimNotify;
        }
    }
    return nullptr;
}

};  // namespace AnimUtils
