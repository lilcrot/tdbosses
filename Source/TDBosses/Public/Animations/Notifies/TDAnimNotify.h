// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"

#include "TDAnimNotify.generated.h"

DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnNotifiedSignature, USkeletalMeshComponent*, UAnimSequenceBase*, const FAnimNotifyEventReference&);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FBP_OnNotifiedSignature,                                                  //
    USkeletalMeshComponent*, SkelMesh, UAnimSequenceBase*, Animation, const FAnimNotifyEventReference&, AnimNotifyRef);  //

UCLASS() class TDBOSSES_API UTDAnimNotify : public UAnimNotify
{
    GENERATED_BODY()

public:
    virtual void Notify(
        USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;

    FOnNotifiedSignature OnNotifed;

    UPROPERTY(BlueprintAssignable)
    FBP_OnNotifiedSignature BP_OnNotified;
};
