// Top Down Bosses. Project for learning

#pragma once
#include "CoreMinimal.h"
#include "Animations/Notifies/TDAnimNotify.h"
#include "TDEndDashAnimNotify.generated.h"

UCLASS()
class TDBOSSES_API UTDEndDashAnimNotify : public UTDAnimNotify
{
    GENERATED_BODY()
};
