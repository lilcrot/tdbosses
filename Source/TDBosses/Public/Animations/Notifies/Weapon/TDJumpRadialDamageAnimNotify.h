// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Animations/Notifies/TDAnimNotify.h"
#include "TDJumpRadialDamageAnimNotify.generated.h"

UCLASS()
class TDBOSSES_API UTDJumpRadialDamageAnimNotify : public UTDAnimNotify
{
    GENERATED_BODY()
};
