// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Animations/Notifies/TDAnimNotify.h"
#include "TDStartMeleeAttackAnimNotify.generated.h"

UCLASS()
class TDBOSSES_API UTDStartMeleeAttackAnimNotify : public UTDAnimNotify
{
    GENERATED_BODY()
};
