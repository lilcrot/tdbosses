// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Animations/Notifies/TDAnimNotify.h"
#include "TDStopSwordTrailAnimNotify.generated.h"

/**
 *
 */
UCLASS()
class TDBOSSES_API UTDStopSwordTrailAnimNotify : public UTDAnimNotify
{
    GENERATED_BODY()
};
