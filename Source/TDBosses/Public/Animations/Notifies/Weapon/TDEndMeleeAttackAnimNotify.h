// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Animations/Notifies/TDAnimNotify.h"
#include "TDEndMeleeAttackAnimNotify.generated.h"

UCLASS()
class TDBOSSES_API UTDEndMeleeAttackAnimNotify : public UTDAnimNotify
{
    GENERATED_BODY()
};
