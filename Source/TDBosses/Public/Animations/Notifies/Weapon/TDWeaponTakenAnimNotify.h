// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Animations/Notifies/TDAnimNotify.h"
#include "TDWeaponTakenAnimNotify.generated.h"

UCLASS()
class TDBOSSES_API UTDWeaponTakenAnimNotify : public UTDAnimNotify
{
    GENERATED_BODY()
};
