// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Animations/Notifies/TDAnimNotify.h"
#include "TDStartSwordTrailAnimNotify.generated.h"

/**
 *
 */
UCLASS()
class TDBOSSES_API UTDStartSwordTrailAnimNotify : public UTDAnimNotify
{
    GENERATED_BODY()
};
