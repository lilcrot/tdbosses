// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "InputActionValue.h"

#include "TDPlayerController.generated.h"

class APlayerCharacter;
class UInputAction;
class ATDBaseWeapon;

UCLASS()
class TDBOSSES_API ATDPlayerController : public APlayerController
{
    GENERATED_BODY()

    ATDPlayerController();

public:
    UFUNCTION()
    void ChangeTargetByInputDirection(const FInputActionValue& Value);

protected:
    AActor* GetBestTarget(TArray<AActor*> Actors, FVector2D Direction = FVector2D(0, 1));

    /*Rotate character with into account weapon offset, so player can fire to target
    if player successfully rotate to target it will return true, e.g if there is an obstacle in front of the player then it will return
    false*/
    UFUNCTION(BlueprintCallable)
    bool RotateCharacterToEnemy(AActor* Target);

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    void OnPlayerStartFire();
    void OnPlayerStopFire();

    virtual void OnPossess(APawn* aPawn) override;
    virtual void OnUnPossess() override;

protected:
    //------------------
    //  PlayerInputs
    //------------------
    UPROPERTY(EditDefaultsOnly, Category = "Inputs|Shooting")
    UInputAction* ChangeTargetAction;

public:
    void SetupPlayerInputToShootingContext(const APlayerCharacter& MyPlayer);  // call only for ranged weapon
    void ResetPlayerInputToDefault(const APlayerCharacter& MyPlayer);
    UEnhancedInputComponent* GetEnhancedInputComponent(const APlayerCharacter& MyPlayer);
};
