// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "TDCharacter.h"
#include "InputActionValue.h"
#include "TDPlayerController.h"

#include "PlayerCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UInputAction;
class UInputMappingContext;
class UAnimMontage;
class UEnhancedInputLocalPlayerSubsystem;

DECLARE_MULTICAST_DELEGATE(FOnEquipActionStart);
DECLARE_MULTICAST_DELEGATE(FOnEquipActionFinished);

UCLASS()
class TDBOSSES_API APlayerCharacter : public ATDCharacter
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    APlayerCharacter();

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    FOnEquipActionStart OnEquipActionStart;
    FOnEquipActionFinished OnEquipActionFinished;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    void Move(const FInputActionValue& Value);

    virtual void OnDeath() override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    USpringArmComponent* SpringArmComponent;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UCameraComponent* CameraComponent;

protected:
    //------------------------------
    // GAS (Gameplay ability system)
    //------------------------------

    virtual void AddStartupGameplayAbilities() override;

    /* Ability that will be bind to input action.
    Note: if abilities point to the same action then only last ability in map will be activate by this input action, other unbinds */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AbilitySet")
    TMap<UInputAction*, TSubclassOf<UGameplayAbility>> InputAbilities;

protected:
    //-----------
    //  Inputs
    //-----------

    void IntilizeInputAbilities();

    UEnhancedInputLocalPlayerSubsystem* InputSubsystem;

protected:  // TDBCharacterContext
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inputs|Default")
    UInputMappingContext* TDBCharacterContext;

    UPROPERTY(EditDefaultsOnly, Category = "Inputs|Default")
    UInputAction* MovementAction;

    UPROPERTY(EditDefaultsOnly, Category = "Inputs|Default")
    UInputAction* ReloadAction;

    UPROPERTY(EditDefaultsOnly, Category = "Inputs|Default")
    UInputAction* NextWeaponAction;

protected:  // ShootingContext
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inputs|Shooting")
    UInputMappingContext* ShootingContext;

    UPROPERTY(EditDefaultsOnly, Category = "Inputs|Shooting")
    UInputAction* DefaultFireAction;

    UPROPERTY(EditDefaultsOnly, Category = "Inputs|Shooting")
    UInputAction* SpecialFireAction;

    friend void ATDPlayerController::SetupPlayerInputToShootingContext(const APlayerCharacter& MyPlayer);
    friend void ATDPlayerController::ResetPlayerInputToDefault(const APlayerCharacter& MyPlayer);
    friend UEnhancedInputComponent* ATDPlayerController::GetEnhancedInputComponent(const APlayerCharacter& MyPlayer);
};
