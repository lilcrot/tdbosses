// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "TargettingNetHelper.generated.h"

/*
 * This actor helps to targetting system for the Actors
 * TargetVolume interacts and saves enemies
 */

class USphereComponent;

UCLASS(NotBlueprintable)
class TDBOSSES_API ATargettingNetHelper : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ATargettingNetHelper();

    /* returns targets that detected (owner isn't taken into account) */
    TArray<AActor*> GetOverlappedTargets() const;

    static ATargettingNetHelper* SpawnTargettingNetHelper(AActor* Owner)
    {
        if (!Owner)
        {
            UE_LOG(LogTemp, Warning, TEXT("Owner is null, for spawn targetting net helper it's necessary!"));
            return nullptr;
        }

        UWorld* World = Owner->GetWorld();
        if (!World)
        {
            UE_LOG(LogTemp, Warning, TEXT("World is null, for spawn targetting net helper it's necessary!"));
            return nullptr;
        }

        FActorSpawnParameters TargettingNetHelperSpawnParams;
        TargettingNetHelperSpawnParams.Owner = Owner;
        return World->SpawnActor<ATargettingNetHelper>(TargettingNetHelperSpawnParams);
    }

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Components")
    USphereComponent* TargetVolume;

    virtual void BeginPlay() override;

    UFUNCTION()
    void OnOwnerEndPlay(AActor* Actor, const EEndPlayReason::Type EndPlayReason);
};
