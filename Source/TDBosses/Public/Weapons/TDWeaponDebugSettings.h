// Top Down Bosses. Project for learning

#pragma once

#include "Engine/DeveloperSettingsBackedByCVars.h"
#include "UObject/NameTypes.h"
#include "UObject/UObjectGlobals.h"

#include "TDWeaponDebugSettings.generated.h"

// clang-format off
UCLASS(config=EditorPerProjectUserSettings)
// clang-format on
class TDBOSSES_API UTDWeaponDebugSettings : public UDeveloperSettingsBackedByCVars
{
    GENERATED_BODY()

public:
    UTDWeaponDebugSettings();

    virtual FName GetCategoryName() const override;

public:
    // Draw debug weapon line trace?
    UPROPERTY(config, EditAnywhere, Category = General, meta = (ConsoleVariable = "TDBosses.Weapon.DrawingMissingLines"))
    bool bDrawDebugWeaponLines;

    // Should we do debug drawing for bullet traces (if above zero, sets how long (in seconds)
    UPROPERTY(config, EditAnywhere, Category = General,                                        //
        meta = (ConsoleVariable = "TDBosses.Weapon.DrawBulletTraceDuration", ForceUnits = s))  //
    float DrawBulletTraceDuration;

    // When bullet line debug drawing is enabled (see DrawBulletTraceDuration), how big should the thickness be? (in cm)
    UPROPERTY(config, EditAnywhere, Category = General, meta = (ConsoleVariable = "TDBosses.Weapon.ThicknessOfTrace", ForceUnits = cm))
    float ThicknessOfTrace;

    // Should we do debug drawing for bullet impacts (if above zero, sets how long (in seconds)
    UPROPERTY(config, EditAnywhere, Category = General, meta = (ConsoleVariable = "TDBosses.Weapon.DrawBulletHitDuration", ForceUnits = s))
    float DrawBulletHitDuration;

    // When bullet hit debug drawing is enabled (see DrawBulletHitDuration), how big should the hit radius be? (in cm)
    UPROPERTY(config, EditAnywhere, Category = General, meta = (ConsoleVariable = "TDBosses.Weapon.SphereHitRadius", ForceUnits = cm))
    float SphereHitRadius;

    // When bullet doesn't hit anyone then draw this line?
    UPROPERTY(config, EditAnywhere, Category = General, meta = (ConsoleVariable = "TDBosses.Weapon.DrawingMissingLines"))
    bool bDrawingMissingLines;
};
