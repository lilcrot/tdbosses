#pragma once

#include "Components/WeaponComponent.h"
#include "Weapons/TDBaseWeapon.h"
#include "TDCoreTypes.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "TDHelper_Weapons.generated.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDHelper_Weapons, All, All);

/* Helper-functions that will make easier work with weapon */
UCLASS()
class TDBOSSES_API UTDHelper_Weapons : public UBlueprintFunctionLibrary
{
public:
    GENERATED_BODY()

    /* Check  weapon component and if invalid then with warning log + exit from editor */
    UFUNCTION(BlueprintPure, Category = "Helper_Weapons")
    static UWeaponComponent* GetWeaponComponentFromActor_Check(const AActor* Actor)
    {
        if (!Actor)
        {
            UE_LOG(LogTDHelper_Weapons, Warning, TEXT("Invalid actor in. GetWeaponComponentFromActor_Check failed!"));
            return nullptr;
        }

        UWeaponComponent* WeaponComponent = Actor->FindComponentByClass<UWeaponComponent>();
        checkf(WeaponComponent, TEXT("%s: Actor that use weapons doesn't has weapon component but must!"), *Actor->GetClass()->GetName());
        return WeaponComponent;
    }

    /* Get weapon component */
    UFUNCTION(BlueprintPure, Category = "Helper_Weapons")
    static UWeaponComponent* GetWeaponComponentFromActor(const AActor* Actor)
    {
        if (!Actor) return nullptr;
        return Actor->FindComponentByClass<UWeaponComponent>();
    }

    UFUNCTION(BlueprintPure, Category = "Helper_Weapons")
    static ATDBaseWeapon* GetCurrentWeaponFromActor(const AActor* Actor)
    {
        UWeaponComponent* WeaponComponent = GetWeaponComponentFromActor(Actor);
        if (!WeaponComponent) return nullptr;
        return WeaponComponent->GetCurrentWeapon();
    }
};
