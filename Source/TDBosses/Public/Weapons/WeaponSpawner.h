// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponSpawner.generated.h"

class UBoxComponent;
class ATDBaseWeapon;
class UStaticMeshComponent;
class ATDCharacter;

/* Gives a weapon to a character or adds ammo to an existing weapon */

UCLASS()
class TDBOSSES_API AWeaponSpawner : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    AWeaponSpawner();

    virtual void Tick(float DeltaTime) override;

protected:
    virtual void BeginPlay() override;

    /* Collision that react to characters and try to give weapon */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponPickup")
    UBoxComponent* CollisionVolume;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponPickup")
    TObjectPtr<UStaticMeshComponent> PadMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponPickup")
    TObjectPtr<UStaticMeshComponent> WeaponMesh;

    /* This class will be spawn */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponPickup")
    TSubclassOf<ATDBaseWeapon> WeaponClass;

    UFUNCTION()
    void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
        bool bFromSwee, const FHitResult& SweepResult);

    bool TryToAddWeaponToCharacter(const AActor* Actor);

    /* if true then weapon will be always respawn */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponPickup")
    bool bInfiniteRespawn;

    /* how many seconds does it take to reset spawn weapon */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponPickup",  //
        meta = (EditCondition = "bInfiniteRespawn", ClampMin = "0.01"))        //
    float RespawnCoolDown;

private:
    FTimerHandle ResetCooldownTimer;
    bool bCanTakeWeapon = true;

    void ResetCooldown();
    bool bResetingCooldown;
};
