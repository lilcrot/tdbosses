// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDCoreTypes.h"
#include "GAS/Abilities/Weapon/GameplayAbility_FireBase.h"
#include "AbilitySystemInterface.h"

#include "TDBaseWeapon.generated.h"

/*       Weapons must be used only with weapon component!
 *       This class only abstract. He mustn't use in bp, see heirs of this class
 *       Fire maked with fire abilities, this helps in game design(you can change places DefaultFire & SpecialFire) and in programming.
 */

class USkeletalMeshComponent;
class UAnimMontage;
class UWeaponComponent;
class UTDAbilitySystemComponent;

UCLASS(Abstract, BlueprintType, NotBlueprintable)
class TDBOSSES_API ATDBaseWeapon : public AActor, public IAbilitySystemInterface
{
    GENERATED_BODY()

    friend class UWeaponComponent;

public:
    // Sets default values for this actor's properties
    ATDBaseWeapon();

    // Attempt to reload, execute reload in current fire ability
    virtual void TryReload();

    // called directly by weapon component
    virtual void OnEquipped();
    virtual void OnUnequipped();
    void OnAttachedToOwner();

    UFUNCTION(BlueprintPure)
    const FWeaponAnimationData& GetWeaponAnimationData() const { return WeaponAnimationData; }

    UFUNCTION(BlueprintPure)
    const FWeaponFireData& GetWeaponFireData() const { return WeaponFireData; }

    UFUNCTION(BlueprintPure)
    USkeletalMeshComponent* GetMesh() { return MeshComponent; }

    UFUNCTION(BlueprintPure)
    const FAmmoData& GetAmmoData() const { return AmmoData; }

    UFUNCTION(BlueprintCallable)
    void SetVisibilityHandle(bool NewVisiblity) const;

    UFUNCTION(BlueprintPure)
    const FTransform GetMuzzleSocketTransform() const;

    UFUNCTION(BlueprintPure, Category = "FireAbilities")
    const FGameplayTag& GetStopFireTag() const { return StopFireTag; };

    /* Get current state: weapon is firing?*/
    UFUNCTION(BlueprintPure)
    bool GetIsFiring() const { return bFiring; }

protected:
    /* with whick tag will be executes stop fire*/
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    FGameplayTag StopFireTag;

    /* These functions will be executes by weapon component. It will never be parallel, weapon component abort those attempts */
    virtual bool DefaultFire();
    virtual bool SpecialFire();
    virtual void StopFire();

    bool bFiring;

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    UPROPERTY(EditDefaultsOnly, Category = "Component")
    USkeletalMeshComponent* MeshComponent;

    AController* GetController() const;

    UPROPERTY(EditDefaultsOnly, Category = "Weapon|Animations")
    FWeaponAnimationData WeaponAnimationData;

    UPROPERTY(EditDefaultsOnly, Category = "Weapon|Fire")
    FWeaponFireData WeaponFireData;

private:
    UPROPERTY(EditDefaultsOnly, Category = "Weapon|Fire")
    FAmmoData AmmoData;

    friend void UGameplayAbility_FireBase::DecreaseAmmo(ATDBaseWeapon& Weapon);
    friend void UGameplayAbility_FireBase::SetCurrentBullets(ATDBaseWeapon& Weapon, const int32 NewValue);

protected:
    //------------------------------
    // GAS (Gameplay ability system)
    //------------------------------

    UFUNCTION(BlueprintPure)
    virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

protected:
    /* Fire abilities*/

    /* Based on this class DefaultFire will be executed by GA */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon|Fire")
    TSubclassOf<UGameplayAbility_FireBase> DefaultFireInfo;

    /* Based on this class SpecialFire will be executed by GA */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon|Fire")
    TSubclassOf<UGameplayAbility_FireBase> SpecialFireInfo;

private:
    bool GiveFireAbility(const TSubclassOf<UGameplayAbility_FireBase> FireInfo);
};
