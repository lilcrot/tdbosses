// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Weapons/TDBaseWeapon.h"

#include "RangedWeapon.generated.h"

UCLASS(Abstract, Blueprintable)
class TDBOSSES_API ARangedWeapon : public ATDBaseWeapon
{
    GENERATED_BODY()
public:
    ARangedWeapon();
};
