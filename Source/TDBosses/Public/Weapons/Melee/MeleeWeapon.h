// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Weapons/TDBaseWeapon.h"
#include "MeleeWeapon.generated.h"

/* This weapon class must work with fire ability that inherited from UMeleeFireAbilityBase. */

class UBoxComponent;
class UNiagaraSystem;
class UNiagaraSystem;
class UNiagaraComponent;

UCLASS(Abstract, Blueprintable)
class TDBOSSES_API AMeleeWeapon : public ATDBaseWeapon
{
    GENERATED_BODY()
public:
    AMeleeWeapon();

    UFUNCTION(BlueprintPure)
    const FVector GetStartPointOfBlade() const;
    UFUNCTION(BlueprintPure)
    const FVector GetEndPointOfBlade() const;

    void OnEndAttack();  // called directly by melee fire abilities
protected:
    virtual void BeginPlay() override;

    // we don't need in this func, melee fire abilities will end attack themselves
    virtual void StopFire() override{};

protected:
    //----------
    //  Visual
    //----------
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visual")
    UNiagaraSystem* TrailNiagaraSystem;

    UNiagaraComponent* TrailNiagaraComponent;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Visual")
    FLinearColor TrailColor;

public:
    void StartTrailVFX();  // binds to anim notify: called directly by melee fire abilities
    void StopTrailVFX();   // binds to anim notify: called directly by melee fire abilities
};
