#pragma once

#include "TDCoreTypes.generated.h"

#define TraceChannel_Weapon ECC_GameTraceChannel1
#define SURFACE_FLESHVULNERABLE SurfaceType1

class UNiagaraSystem;
class UPhysicalMaterial;
class UGameplayAbility;
class UInputAction;

USTRUCT(BlueprintType)
struct FAmmoData
{
    GENERATED_USTRUCT_BODY()

    /*if true then weapon don't need reload*/
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
    bool bInfinite = false;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (EditCondition = "!bInfinite"))
    int32 MaxBullets = 20;

    int32 GetCurrentBullets() const { return CurrentBullets; }

    void SetCurrentBullets(int32 NewCount)
    {
        if (bInfinite) return;
        NewCount > MaxBullets ? CurrentBullets = MaxBullets : NewCount < 0 ? CurrentBullets = 0 : CurrentBullets = NewCount;
    }
    void DecreaseCurrentBullets()
    {
        if (bInfinite) return;
        SetCurrentBullets(CurrentBullets - 1);
    }

    /*how many seconds need to finally reload*/
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (EditCondition = "!bInfinite", ClampMin = "0.01", Units = "s"))
    float ReloadingTime = 2.5f;

private:
    int32 CurrentBullets;
};

UENUM(BlueprintType)
enum class EWeaponTypes : uint8
{
    None,
    Melee UMETA(DisplayName = "Melee Weapon"),
    Ranged UMETA(DisplayName = "Ranged Weapon")
};

USTRUCT(BlueprintType)
struct FMuzzleFlash
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    UNiagaraSystem* VFX = nullptr;

    /*Name of socket that vfx will be attached to*/
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    FName SocketName;
};

USTRUCT(BlueprintType)
struct FWeaponFireData
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    EWeaponTypes Type = EWeaponTypes::None;

    /*With which rate weapon must fire*/
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ClampMin = "0.01", Units = "s"))
    float ShootRate = 0.2f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    bool bAutoFire = false;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (EditCondition = "Type == EWeaponTypes::Ranged"))
    FName MuzzleSocketName = "Muzzle";

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (EditCondition = "Type == EWeaponTypes::Ranged"))
    UNiagaraSystem* ProjectileNiagaraSystem = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (EditCondition = "Type == EWeaponTypes::Ranged"))
    FMuzzleFlash MuzzleFlash;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    UNiagaraSystem* DefaultImpactNiagara = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    TMap<UPhysicalMaterial*, UNiagaraSystem*> ImpactMap;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (EditCondition = "Type == EWeaponTypes::Melee"))
    FName BladeStartSocketName = "BladeStart";

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (EditCondition = "Type == EWeaponTypes::Melee"))
    FName BladeEndSocketName = "BladeEnd";

    /*With which range weapon must shoot*/
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (EditCondition = "Type == EWeaponTypes::Ranged", Units = "cm"))
    float ShotRange = 3000.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ClampMin = 0))
    int32 DamageAmount = 1;
};

USTRUCT(BlueprintType)
struct FWeaponAnimationData
{
    GENERATED_USTRUCT_BODY()

    /*ABP that will be link to character*/
    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<UAnimInstance> EquipAnimLayer;

    UPROPERTY(EditDefaultsOnly)
    UAnimMontage* EquipMontage = nullptr;

    UPROPERTY(EditDefaultsOnly)
    UAnimMontage* ReloadMontage = nullptr;
};

USTRUCT(BlueprintType)
struct FWeaponEquipAttachSocket
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    FName SocketName;

    // attach on right hand?
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    bool bIsRightHand = true;
};
