// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "GAS/Abilities/TDGameplayAbility.h"

#include "TDAbilitySystemComponent.generated.h"

class UInputAction;

USTRUCT()
struct FAbilityInputBinding
{
    GENERATED_USTRUCT_BODY()

    int32 InputID = 0;
    uint32 OnPressedHandle = 0;
    uint32 OnReleasedHandle = 0;
    FGameplayAbilitySpecHandle BoundAbility;
};

UCLASS()
class TDBOSSES_API UTDAbilitySystemComponent : public UAbilitySystemComponent
{
    GENERATED_BODY()

public:
    virtual void BeginPlay() override;

    virtual void NotifyAbilityActivated(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability) override;
    virtual void NotifyAbilityEnded(FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability, bool bWasCancelled) override;

    //---------------------------
    //  Ability ActivatingPolicy
    //---------------------------

    /* return true if activation group can't will be activated right now */
    UFUNCTION(BlueprintPure)
    bool IsActivationGroupBlocked(EAbilityActivationGroup Group) const;

    /* return number of abilities with transfered group that running right now */
    UFUNCTION(BlueprintPure)
    int32 GetActivationCountsByGroup(EAbilityActivationGroup Group) const { return ActivationGroupCounts[(uint8)Group]; };

    void AddAbilityToActivationGroup(EAbilityActivationGroup Group, UTDGameplayAbility* TDAbility);
    void RemoveAbilityFromActivationGroup(EAbilityActivationGroup Group, UTDGameplayAbility* TDAbility);
    void CancelActivationGroupAbilities(EAbilityActivationGroup Group, UTDGameplayAbility* IgnoreAbility, bool bReplicateCancelAbility);

    typedef TFunctionRef<bool(const UTDGameplayAbility* LyraAbility, FGameplayAbilitySpecHandle Handle)> TShouldCancelAbilityFunc;
    void CancelAbilitiesByFunc(TShouldCancelAbilityFunc ShouldCancelFunc, bool bReplicateCancelAbility);

private:
    // Number of abilities running in each activation group.
    int32 ActivationGroupCounts[(uint8)EAbilityActivationGroup::MAX];

public:
    //-------------------------
    // Enhanced Inputs Binding
    //-------------------------

    /* Bind ability to this input action. If aready have bind then delete old bind and rebind to new */
    UFUNCTION(BlueprintCallable, Category = "EnhancedInput Abilities")
    void SetInputBinding(UInputAction* InputAction, const FGameplayAbilitySpecHandle& AbilityHandle);

    /* Remove gampelay ability bind from input action that he is bound */
    UFUNCTION(BlueprintCallable, Category = "EnhancedInput Abilities")
    void ClearAbilityInputBinding(const FGameplayAbilitySpecHandle& AbilityHandle);

protected:
    void ClearAbilityBindingFromInputAction(const UInputAction* InputAction);

    void TryBindAbilityInput(UInputAction* InputAction, FAbilityInputBinding& AbilityInputBinding);

    void OnAbilityInputPressed(UInputAction* InputAction);
    void OnAbilityInputReleased(UInputAction* InputAction);

    UPROPERTY(transient)
    TMap<UInputAction*, FAbilityInputBinding> MappedAbilities;

    UPROPERTY(transient)
    UEnhancedInputComponent* InputComponent;
};
