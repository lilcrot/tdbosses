// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "TDGameplayAbility.generated.h"

// Defines how an ability activates in relation to other abilities.
UENUM(BlueprintType)
enum class EAbilityActivationGroup : uint8
{
    // Ability runs independently of all other abilities.
    Independent,

    // Ability is canceled and replaced by other exclusive abilities.
    Exclusive_Replaceable,

    // Ability blocks all other exclusive abilities from activating.
    Exclusive_Blocking,

    MAX UMETA(Hidden)
};

/* Implement visual on bp, on cpp logic with rare exceptions */
UCLASS(Abstract, NotBlueprintType, Blueprintable)
class TDBOSSES_API UTDGameplayAbility : public UGameplayAbility
{
    GENERATED_BODY()
public:
    UTDGameplayAbility();

    UFUNCTION(BlueprintPure, Category = "Ability Activation")
    EAbilityActivationGroup GetActivationGroup() const { return ActivationGroup; }

protected:
    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags,
        FGameplayTagContainer* OptionalRelevantTags) const override;

    /*Note: Logging warning msg and end ability but will NOT exit of function(where this helper func executes), you must to do it yourself*/
    void ForcedEndAbilityWithWarningLog(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility = true, const FString& Msg = "");

    /* Wrap function for ForcedEndAbilityWithWarningLog with current end info */
    void ForcedCurEndAbilityWithWarningLog(bool bReplicateEndAbility = true, const FString& Msg = "");

    void EndAbilityWithCurrentInfo(bool bReplicateEndAbility = true, bool bWasCancelled = true);

    // Defines the relationship between this ability activating and other abilities activating.
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability Activation")
    EAbilityActivationGroup ActivationGroup;
};
