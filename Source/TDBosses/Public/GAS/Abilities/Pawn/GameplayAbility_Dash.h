// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/TDGameplayAbility.h"
#include "GameplayAbility_Dash.generated.h"

class UAnimMontage;

UCLASS()
class TDBOSSES_API UGameplayAbility_Dash : public UTDGameplayAbility
{
    GENERATED_BODY()
public:
    UGameplayAbility_Dash();

    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

protected:
    /* Dash strength that will applied to root motion */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dash")
    float DashStrength;

    /* Must be less than AbilityDuration */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dash", meta = (Units = "s"))
    float RootMotionDuration;

    /* Must be more than RootMotionDuration */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dash", meta = (Units = "s"))
    float AbilityDuration;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dash")
    UAnimMontage* DashMontage;

    /* do NOT play dash montage this already done in cpp */
    UFUNCTION(BlueprintNativeEvent)
    void DashVisual();

private:
    UFUNCTION()
    void OnApplyRootMotionFinished();

    UFUNCTION()
    void ForcedEndAbility();
};
