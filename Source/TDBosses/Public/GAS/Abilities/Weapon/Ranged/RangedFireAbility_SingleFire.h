// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/Weapon/Ranged/GameplayAbility_RangedFireBase.h"

#include "RangedFireAbility_SingleFire.generated.h"

UCLASS(Abstract)
class TDBOSSES_API URangedFireAbility_SingleFire : public UGameplayAbility_RangedFireBase
{
    GENERATED_BODY()
public:
    URangedFireAbility_SingleFire();

protected:
    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled);

    UFUNCTION()
    void OnStopFireTagRecieved(FGameplayEventData Payload);

    void Fire();
    FTimerHandle FireHandle;

    /* Executes automaticly, just override and make any VFX */
    UFUNCTION(BlueprintNativeEvent)
    void PlayEffects(const FHitResult& Hit);
};
