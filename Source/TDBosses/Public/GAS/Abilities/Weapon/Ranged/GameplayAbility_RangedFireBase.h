#pragma once
// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/Weapon/GameplayAbility_FireBase.h"

#include "GameplayAbility_RangedFireBase.generated.h"

class ARangedWeapon;

UCLASS(Abstract)
class TDBOSSES_API UGameplayAbility_RangedFireBase : public UGameplayAbility_FireBase
{
    GENERATED_BODY()
public:
    UGameplayAbility_RangedFireBase();

    UFUNCTION(BlueprintPure)
    ARangedWeapon* GetRangedWeapon();

protected:
    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

    // return true if successfully  get data
    virtual bool GetTraceData(
        FVector& TraceStart, FVector& TraceEnd, FVector& ShotDirection, FCollisionQueryParams& CollisionQueryParams) const;

    virtual bool CanShot() const;
};
