// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/Weapon/Ranged/GameplayAbility_RangedFireBase.h"

#include "RangedFireAbility_QuickFire.generated.h"

UCLASS(Abstract)
class TDBOSSES_API URangedFireAbility_QuickFire : public UGameplayAbility_RangedFireBase
{
    GENERATED_BODY()
public:
    URangedFireAbility_QuickFire();

protected:
    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled);

    void QuickFire();

    void ShotWithDirectionDelta(const float DeltaDirection = 0.0f);

    bool GetQuickFireTraceData(FVector& TraceStart, FVector& TraceEnd, FVector& ShotDirection,  //
        FCollisionQueryParams& CollisionQueryParams, const float DeltaDirection) const;         //

    /*  note: count of elems = shot count, e.g if 5 elems => 5 shot
        tip: by default shot to forward isn't implied so make one argument with zero*/
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Fire")
    TArray<float> DirectionDeggreeDeltas{0.0f, 20.0f, -20.0f};

    /* Plus this value with default shot rate of weapon if you want more balance ability */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Fire", meta = (Units = "s"))
    float PlusShotRate = 0.0f;

    /* Executes automaticly, just override and make any VFX */
    UFUNCTION(BlueprintNativeEvent)
    void PlayEffects(const FHitResult& Hit, const float DeltaDirectionDegree);  // @param DeltaDirectionDegree: count from muzzle

private:
    UFUNCTION()
    void OnStopFireTagRecieved(FGameplayEventData Payload);

    FTimerHandle FireHandle;
};
