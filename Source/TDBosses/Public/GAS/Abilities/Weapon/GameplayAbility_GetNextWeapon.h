// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/Weapon/GameplayAbility_SelectCurrentWeapon.h"
#include "Components/WeaponComponent.h"

#include "GameplayAbility_GetNextWeapon.generated.h"

UCLASS()
class TDBOSSES_API UGameplayAbility_GetNextWeapon : public UGameplayAbility_SelectCurrentWeapon
{
    GENERATED_BODY()
protected:
    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
};
