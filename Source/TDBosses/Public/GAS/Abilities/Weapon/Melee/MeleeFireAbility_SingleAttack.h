// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/Weapon/Melee/GameplayAbility_MeleeFireBase.h"
#include "MeleeFireAbility_SingleAttack.generated.h"

UCLASS(Abstract)
class TDBOSSES_API UMeleeFireAbility_SingleAttack : public UGameplayAbility_MeleeFireBase
{
    GENERATED_BODY()
public:
    UMeleeFireAbility_SingleAttack();

protected:
    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual void OnNotifyStartAttack(
        USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;
    virtual void OnNotifyEndAttack(
        USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;

private:
    FTimerHandle LineTraceTimer;
};
