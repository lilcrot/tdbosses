// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/Weapon/Melee/GameplayAbility_MeleeFireBase.h"

#include "MeleeFireAbility_JumpRadial.generated.h"

UCLASS(Abstract)
class TDBOSSES_API UMeleeFireAbility_JumpRadial : public UGameplayAbility_MeleeFireBase
{
    GENERATED_BODY()
public:
    UMeleeFireAbility_JumpRadial();

protected:
    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual void OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
    float DamageRadius;

    /* Plus this value with default damage amount of weapon if you want more balance ability */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
    float PlusDamageAmount;

    /* apply full damage from center of radial radius? */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
    bool bApplyFullRadialDamage;

    virtual void OnNotifyStartAttack(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,  //
        const FAnimNotifyEventReference& EventReference) override;                                    //

    virtual void OnNotifyEndAttack(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,  //
        const FAnimNotifyEventReference& EventReference) override;                                  //

    UFUNCTION()
    void OnNotifyJumpRadialDamage(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,  //
        const FAnimNotifyEventReference& EventReference);                                          //

    /* Executes automaticly, just override and make any VFX */
    UFUNCTION(BlueprintNativeEvent)
    void PlayEffects(const FVector& Origin);
};
