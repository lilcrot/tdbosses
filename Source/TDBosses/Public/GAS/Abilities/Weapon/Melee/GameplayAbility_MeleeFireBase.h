// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/Weapon/GameplayAbility_FireBase.h"
#include "TDCoreTypes.h"

#include "GameplayAbility_MeleeFireBase.generated.h"

class AMeleeWeapon;

UCLASS(Abstract)
class TDBOSSES_API UGameplayAbility_MeleeFireBase : public UGameplayAbility_FireBase
{
    GENERATED_BODY()
public:
    UGameplayAbility_MeleeFireBase();

    UFUNCTION(BlueprintPure)
    const AActor* GetPrevDamagedTarget() const { return PrevDamagedTarget; }

    UFUNCTION(BlueprintPure)
    UAnimMontage* GetCharacterAttackMontage() const { return CharacterAttackMontage; }

    UFUNCTION(BlueprintPure)
    AMeleeWeapon* GetMeleeWeapon();

protected:
    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

    virtual void OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
    virtual void OnRemoveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

    /* functions that attached to requireds anim notifies (CharacterAttackMontage) */
    UFUNCTION()
    virtual void OnNotifyStartAttack(
        USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference);
    UFUNCTION()
    virtual void OnNotifyEndAttack(
        USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference);

    void LoopMeleeSphereTrace();
    virtual void TryApplyAttack(const FHitResult& Hit, AActor* const Target, AActor* const Instigator);

    UPROPERTY()
    const AActor* PrevDamagedTarget;

    FVector GetStartPointOfBlade();
    FVector GetEndPointOfBlade();

    /* Binds to character attack montage */
    UFUNCTION()
    void OnAttackRootMotionFinished();

    /* Binds to character attack montage */
    UFUNCTION()
    void ForcedEndAbility();

protected:
    // -------------
    // CharacterAttackMontage | Visual
    // -------------

    /* this montage will play on character that own this fire ability*/
    UPROPERTY(EditDefaultsOnly, Category = "FireAbility|Animations")
    UAnimMontage* CharacterAttackMontage;

private:
    void OnStartTrailVFX(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference);
    void OnStopTrailVFX(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference);
};
