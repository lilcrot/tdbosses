// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/TDGameplayAbility.h"
#include "TDCoreTypes.h"

#include "GameplayAbility_FireBase.generated.h"

class USkeletalMeshComponent;
class ACharacter;
class ATDBaseWeapon;
class UAnimMontage;

/* By default those fire abilities will own not a weapon but actor that own weapons (for the purposes of high performance)!
   Weapons will have all info of abilities and TryActivate them when input action triggered by actor that own them.
*/

UCLASS()
class TDBOSSES_API UGameplayAbility_FireBase : public UTDGameplayAbility
{
    GENERATED_BODY()
public:
    void DecreaseAmmo(ATDBaseWeapon& Weapon);
    void SetCurrentBullets(ATDBaseWeapon& Weapon, const int32 NewValue);

    UGameplayAbility_FireBase();

protected:
    virtual void PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate,
        const FGameplayEventData* TriggerEventData = nullptr) override;

    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

    UFUNCTION(BlueprintPure)
    ATDBaseWeapon* GetCurrentWeapon() const;

    void DrawWeaponDebug(const FHitResult& Hit, const FVector& TraceStart, const FVector& TraceEnd);

    /* return true if successfully applied damage */
    UFUNCTION(BlueprintCallable)
    bool MakePointDamage(const FHitResult& Hit, const FVector& ShotDirection, const float DamageAmount, AActor* Instigator);

    /* return true if successfully applied damage */
    bool ApplyRadialDamage(const FVector& Origin, const float DamageRadius, bool bDoFullDamage, AActor* Instigator,  //
        const float Damage = -1.0f, ECollisionChannel DamageChannel = TraceChannel_Weapon);                          //
private:
    ATDBaseWeapon* CurrentWeapon;
};
