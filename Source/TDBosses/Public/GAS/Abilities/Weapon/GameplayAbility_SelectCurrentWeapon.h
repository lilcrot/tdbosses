// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "GAS/Abilities/TDGameplayAbility.h"

#include "GameplayAbility_SelectCurrentWeapon.generated.h"

class UWeaponComponent;

UCLASS()
class TDBOSSES_API UGameplayAbility_SelectCurrentWeapon : public UTDGameplayAbility
{
    GENERATED_BODY()
public:
    UGameplayAbility_SelectCurrentWeapon();

protected:
    virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

    virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
        const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

    UFUNCTION()
    void OnEquipFinished();

    UFUNCTION()
    void ForcedEndAbility();

    void SelectNextWeapon(UWeaponComponent& WeaponComponent);
    void SelectWeapon(UWeaponComponent& WeaponComponent, const int32 WeaponIndex);
    bool WillBeTakenNewWeapon(const UWeaponComponent& WeaponComponent, const int32 NewWeaponIndex) const;
};
