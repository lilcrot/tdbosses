// Top Down Bosses. Project for learning

#pragma once
#include "Tests/Utils/InputRecordingUtils.h"
#include "CoreMinimal.h"

struct FInputActionInstance;

namespace Test
{
class JsonUtils
{
public:
    static bool WriteInputData(const FString& FileName, const FInputData& InputData);
    static bool ReadInputData(const FString& FileName, FInputData& InputData);
};

}  // namespace Test
