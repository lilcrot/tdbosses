#pragma once
#include "CoreMinimal.h"
#include "Tests/TestUtils.h"
#include "Misc/AutomationTest.h"
#include "Tests/AutomationCommon.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedPlayerInput.h"

class FSimulateInputActionsLatentCommand : public IAutomationLatentCommand
{
public:
    FSimulateInputActionsLatentCommand(UWorld* InWorld, UEnhancedInputComponent* InInputComponent, UEnhancedPlayerInput* InPlayerInput,
        const TArray<FBindingsData>& InBindingsData)
        : World(InWorld), EnhancedInputComponent(InInputComponent), BindingsData(InBindingsData), PlayerInput(InPlayerInput)
    {
        checkf(World, TEXT("SimulateInputActionsLatentCommand: World is nullptr"));
        checkf(EnhancedInputComponent, TEXT("SimulateInputActionsLatentCommand: EnhancedInputComponent is nullptr"));
        checkf(PlayerInput, TEXT("SimulateInputActionsLatentCommand: EnhancedPlayerInput is nullptr"));
    }

    virtual bool Update() override
    {
        if (!World || !EnhancedInputComponent || !PlayerInput) return true;

        if (WorldStartTime == 0.0f)
        {
            WorldStartTime = World->TimeSeconds;
        }

        TArray<TUniquePtr<FEnhancedInputActionEventBinding>> TriggeredDelegates;
        while (World->TimeSeconds - WorldStartTime >= BindingsData[Index].WorldTime)
        {
            for (const TUniquePtr<FEnhancedInputActionEventBinding>& Binding : EnhancedInputComponent->GetActionEventBindings())
            {
                const ETriggerEvent BoundTriggerEvent = Binding->GetTriggerEvent();
                TMap<FName, FActionData> ActionDataMap;

                for (int32 j = 0; j < BindingsData[Index].ActionsData.Num(); ++j)
                {
                    if (GetActionName(Binding->GetAction()) != BindingsData[Index].ActionsData[j].ActionName) continue;

                    const ETriggerEvent CurrentTrigger = BindingsData[Index].ActionsData[j].CurrentTrigger;

                    if (BoundTriggerEvent == CurrentTrigger ||
                        (BoundTriggerEvent == ETriggerEvent::Started &&
                            CurrentTrigger == ETriggerEvent::Triggered))  // InputRecording doesn't serialize TriggerEvent::Started so it's
                                                                          // our path to solve
                    {
                        // Record intent to trigger started as well as triggered
                        // EmplaceAt 0 for the "Started" event it is always guaranteed to fire before Triggered
                        if (BoundTriggerEvent == ETriggerEvent::Started)
                        {
                            TriggeredDelegates.EmplaceAt(0, Binding->Clone());
                        }
                        else
                        {
                            TriggeredDelegates.Emplace(Binding->Clone());
                        }
                        ActionDataMap.Emplace(GetActionName(Binding->GetAction()), BindingsData[Index].ActionsData[j]);
                    }
                }
                for (TUniquePtr<FEnhancedInputActionEventBinding>& Delegate : TriggeredDelegates)
                {
                    TObjectPtr<const UInputAction> DelegateAction = Delegate->GetAction();
                    if (ActionDataMap.Contains(GetActionName(DelegateAction)))
                    {
                        PlayerInput->InjectInputForAction(DelegateAction,                            //
                            FInputActionValue(ActionDataMap[GetActionName(DelegateAction)].Value));  //
                    }
                }

                TriggeredDelegates.Reset();
                ActionDataMap.Reset();
            }
            if (++Index >= BindingsData.Num()) return true;
        }

        return false;
    }

private:
    FName GetActionName(const UInputAction* Action) const { return Action ? Action->GetFName() : FName(); }

    const UWorld* World;
    const UEnhancedInputComponent* EnhancedInputComponent;
    const TArray<FBindingsData> BindingsData;
    UEnhancedPlayerInput* PlayerInput;
    int32 Index{0};
    float WorldStartTime{0.0f};
};
