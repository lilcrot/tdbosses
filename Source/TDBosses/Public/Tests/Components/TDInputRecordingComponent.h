// Top Down Bosses. Project for learning

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Tests/Utils/InputRecordingUtils.h"

#include "TDInputRecordingComponent.generated.h"

/*------------TDInputRecordingComponent------------
 *
 * This component save all enhanced inputs from devices(e.g keyboard) to json file with parametrized name,
 * to "Saved/Tests/{FileName}.json" path by default.
 * It can use in automation tests, e.g in PlayerTests
 *
 *------------TDInputRecordingComponent------------*/

class UEnhancedPlayerInput;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDBOSSES_API UTDInputRecordingComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UTDInputRecordingComponent();

protected:
    virtual void BeginPlay() override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

    UPROPERTY(EditAnywhere)
    FString FileName{"CharacterTestInput"};

public:
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
    UPROPERTY()
    UEnhancedPlayerInput* EnhancedPlayerInput;

    FInputData InputData;

    FBindingsData MakeBindingsData() const;
    FString GenerateFileName() const;
};
